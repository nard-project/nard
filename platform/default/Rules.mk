
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2022 Ronny Nilsson
#
# default rules used by all products


#-----------------------------
# Define all targets common
# variables
ifndef TMP
	export TMP = $(PATH_SCRAP)
endif
ifndef TMPDIR
	export TMPDIR = $(PATH_SCRAP)
endif


# Find the path to the cross compiler
# used by this plattform once it has
# been built. Variables is defined in
# generated makefiles in $(PATH_INTER)/
ifdef CROSS_TUPLE
	export PATH_CROSS_TOOLCHAIN := $(CROSS_TOOLCHAIN)
	export PATH := $(PATH_CROSS_TOOLCHAIN)/bin:$(PATH):.
	export ARCH := $(firstword $(subst -, ,$(CROSS_TUPLE)))

	# Default compiler options for applications
	ifndef CROSS_CFLAGS_DEFAULT													# Only append flags once
		export CROSS_CFLAGS_DEFAULT := 1
		CROSS_CFLAGS += -Wall -ggdb3 -O2 -fno-omit-frame-pointer
		CROSS_CFLAGS += -pipe -fno-optimize-sibling-calls
		CROSS_CFLAGS += -fno-strict-aliasing -fno-crossjumping
		CROSS_CFLAGS += -falign-functions -fno-short-enums
		CROSS_CFLAGS += $(CROSS_TUNE) $(CROSS_ARCH) $(CROSS_FPU)
		ifneq ($(findstring ARM,$(ARCH))$(findstring arm,$(ARCH)),)				# Do we build for ARM? Then add some ARM options.
			CROSS_CFLAGS += -mapcs-frame -mno-sched-prolog -mabi=aapcs-linux
			CROSS_CFLAGS += -mno-unaligned-access
		endif
	endif
	export CROSS_CFLAGS
	export CROSS_CXXFLAGS = $(CROSS_CFLAGS)
endif
export PATH_CROSS_CC := $(PATH_CROSS_TOOLCHAIN)/bin/$(CROSS_TUPLE)-
export PATH_CROSS_LIBC := $(PATH_CROSS_TOOLCHAIN)/$(CROSS_TUPLE)/sysroot
export PATH_SYSROOT := $(PATH_CROSS_LIBC)
export PATH_DEBUGROOT := $(PATH_CROSS_TOOLCHAIN)/$(CROSS_TUPLE)/debug-root
export CONFIG_SYSROOT := $(PATH_CROSS_LIBC)
export CROSS_CC := $(PATH_CROSS_CC)gcc
export CROSS_CXX := $(PATH_CROSS_CC)g++
export CROSS_OBJCOPY := $(PATH_CROSS_CC)objcopy
export CROSS_OBJDUMP := $(PATH_CROSS_CC)objdump
export CROSS_LD := $(PATH_CROSS_CC)ld
export CROSS_STRIP := $(PATH_CROSS_CC)strip
export CROSS_AR := $(PATH_CROSS_CC)ar
export CROSS_RANLIB := $(PATH_CROSS_CC)ranlib
export CROSS_SIZE := $(PATH_CROSS_CC)size
export CROSS_NM := $(PATH_CROSS_CC)nm
export PKG_CONFIG := ""
export MANIFEST_TOOL := /bin/true


# Flag whether the Nard add-on is available or not.
ifneq ($(wildcard $(PATH_APPS)/videocore-libs/Rules.mk),)
	export HAS_NARD_ADDONS := 1
endif



#-----------------------------
# Generate the board.h include file which applications
# can include if they staticaly want to know which
# product and board HW we build for. (This info is 
# available in runtime too, in /etc/product and /etc/board.)
# We need a protective conditional here or Make will complain
# due to this file is re-included several times during
# a build.
ifndef BOARD_H_RULE
ifdef PRODUCT
	BOARD_H_RULE := dummy
$(PATH_INTER)/board.h: $(PATH_INTER)/board.mk
	@if test -z "$(strip $(BOARD))"; then										\
		echo "*********************************************";					\
		echo -n "Error, no BOARD defined! Your product recipe is invalid. ";	\
		echo -e "Ensure that:\n\tnard/platform/$(MAKECMDGOALS)/Rules.mk";		\
		echo -e "contains the lines:\n\tPRODUCT_DEPS += skeleton";				\
		echo -e "\t-include \$$(PATH_TOP)/platform/skeleton/Rules.mk";			\
		echo "*********************************************";					\
		exit 1;																	\
	fi
	echo "/* Autogenerated file, describes built board & product. */" >"$(PATH_INTER)/board.h.tmp"
	echo "/* One define for each board dependancy. */" >>"$(PATH_INTER)/board.h.tmp"
	echo "#ifndef PRODUCT" >>"$(PATH_INTER)/board.h.tmp"
	echo -n "#define PRODUCT " >>"$(PATH_INTER)/board.h.tmp"
	echo "$(PRODUCT)" | tr "[[:lower:]-]" "[[:upper:]_]" >>"$(PATH_INTER)/board.h.tmp"
	echo -n "#define PRODUCT_" >>"$(PATH_INTER)/board.h.tmp"
	echo "$(PRODUCT)" | tr "[[:lower:]-]" "[[:upper:]_]" >>"$(PATH_INTER)/board.h.tmp"
	echo -n "#define " >>"$(PATH_INTER)/board.h.tmp"
	echo "$(PRODUCT)" | tr "[[:lower:]-]" "[[:upper:]_]" >>"$(PATH_INTER)/board.h.tmp"
	echo -n "#define BOARD " >>"$(PATH_INTER)/board.h.tmp"
	echo "$(BOARD)" | tr "[[:lower:]-]" "[[:upper:]_]" >>"$(PATH_INTER)/board.h.tmp"
	echo -n "#define " >>"$(PATH_INTER)/board.h.tmp"
	echo "$(BOARD)" | tr "[[:lower:]-]" "[[:upper:]_]" >>"$(PATH_INTER)/board.h.tmp"
	for B in $(BOARD_DEPS); do							\
		echo -n "#define BOARD_";						\
		echo "$$B" | tr "[:lower:]-" "[:upper:]_";		\
	done >>$(PATH_INTER)/board.h.tmp
	echo "#endif" >>"$(PATH_INTER)/board.h.tmp"
	mv -vf "$(PATH_INTER)/board.h.tmp" "$(PATH_INTER)/board.h"
endif
endif
