#!/bin/ash

# initramfs startup script
# Ensures rootfs is of type tmpfs.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2019 Ronny Nilsson


# -------------------------------------------------------------
init() {
	# Setup paths
	PATH_NEWROOT="/newroot"
	CMDPREFIX="$PATH_NEWROOT/lib/ld-linux.so \
		--inhibit-cache \
		--library-path $PATH_NEWROOT/lib \
		$PATH_NEWROOT/bin/busybox"

	needNewRoot=0
}



# -------------------------------------------------------------
# We are in the boot process and the kernel is still probing
# HW for devices. We need to monitor the automatic /dev, but
# before mounting it we preserve static content from the
# initramfs.
mount_pseudos_move_dev() {
	/bin/mv -f /dev /dev.template
	/bin/mkdir -p /dev
	/bin/mount -t devtmpfs devtmpfs /dev
	/bin/mv -n /dev.template/* /dev
	/bin/rm -rf /dev.template

	/bin/mount -t sysfs sysfs /sys
	/bin/mount -t proc proc /proc
}



# -------------------------------------------------------------
umount_pseudos() {
	/bin/umount /sys /dev /proc
}



# -------------------------------------------------------------
# We need to wait for the HW enumeration to complete before
# moving the entire root filesystem.
wait_hw_enumeration() {
	local delay tmp1 tmp2


	delay=0
	until test -b /dev/mmcblk0; do
		delay=$((delay + 1))
		test $delay -eq 100 && break
		sleep 0.2
		echo -n "."
	done

	delay=0
	tmp1=0
	tmp2=0
	until test $delay -eq 4; do
		tmp1=$(find /sys/class /sys/bus | wc -l)
		test $tmp1 -eq $tmp2 || delay=0
		tmp2=$tmp1

		delay=$((delay + 1))
		sleep 0.2
		echo -n "."
	done
}



# -------------------------------------------------------------
# Find out how large our root filesystem is
# and limit the root tmpfs in size.
setRootSize() {
	local size

	size=$(/usr/bin/du -sxk / | /usr/bin/awk '{ print int ($1 * 2.1 + 51200) }')

	if [ $needNewRoot -eq 1 ]; then
		/bin/mkdir -p $PATH_NEWROOT
		/bin/chmod 0755 $PATH_NEWROOT
		/bin/mount -t tmpfs tmpfs $PATH_NEWROOT -o size=${size}k,mode=755
	else
		/bin/mount / -o remount,size=${size}k,mode=755
		/bin/chmod 0755 /														# mount-mode option does not work
	fi
}



# -------------------------------------------------------------
# Copy and move everything (almost) from current
# root filesystem into a new filesystem.
move_everything() {
	echo -n "."
	cp -fa /bin $PATH_NEWROOT
	echo -n "."
	cp -fa /lib $PATH_NEWROOT

	echo -n "."
	$CMDPREFIX rm -rf /bin /lib
	hash -r

	# By now we need to execute BusyBox from
	# the new root only.
	for D in $($CMDPREFIX find / -xdev -mindepth 1 -maxdepth 1 | \
			$CMDPREFIX grep -vE "/bin|/lib|$PATH_NEWROOT|/init"); do
		$CMDPREFIX echo -n "."
		$CMDPREFIX mv -f $D $PATH_NEWROOT
	done
}



# -------------------------------------------------------------
# Verify that our recently unpacked initramfs is intact,
# due to it might have become broken during PXE download.
check_rootfs_csum() {
	echo "Checking rootfs integrity..."
	if ! (cd / && /usr/bin/md5sum -swc "/lib/nard/fs.md5"); then
		echo "Error, rootfs is corrupt! Rebooting now."
		/sbin/reboot -d 5 -f
	fi

	return 0
}



# -------------------------------------------------------------
init
mount_pseudos_move_dev

# Are the roofs of type ramfs or tmpfs?
/bin/grep -q "^rootfs.*size=" /proc/mounts || needNewRoot=1

# If rootfs already is tmpfs we only need to limit the
# size and we're done.
if [ $needNewRoot -eq 0 ]; then
	echo
	echo "Kernel startup finished..."
	check_rootfs_csum
	setRootSize
	umount_pseudos
	exec /sbin/init
fi


# If rootfs is ramfs we need to create a tmpfs and move
# everything into it.
echo
echo "Kernel startup finished, now waiting for hotplug events..."
check_rootfs_csum
wait_hw_enumeration
setRootSize
umount_pseudos
move_everything

# Finished, now run real init
$CMDPREFIX echo "."
exec $CMDPREFIX switch_root $PATH_NEWROOT /sbin/init

