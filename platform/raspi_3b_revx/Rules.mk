
#-------------------------------------------
# Common hardware recipe for products
# running on Raspberry Pi model 3B
#-------------------------------------------

# Default compiler options for applications. Here we depend
# on which compiler (toolchain) is in use. The oldest one
# doesn't know of ARM Cortex-A53, but later does.
ifneq ($(CROSS_TOOLCHAIN_VER),1.18.0)
    CROSS_TUNE ?= -march=armv8-a+crc -mtune=cortex-a53
    CROSS_ARCH ?= -D__LINUX_ARM_ARCH__=8
    CROSS_FPU ?= -mfpu=crypto-neon-fp-armv8 -mfloat-abi=hard -mhard-float
endif



#-----------------------------
# Get defines which tell what hw and
# sw we use and thus need (if any).
BOARD_DEPS += raspi_2b_revx
-include $(PATH_TOP)/platform/raspi_2b_revx/Rules.mk

