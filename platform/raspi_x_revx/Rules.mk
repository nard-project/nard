
#-------------------------------------------
# Common hardware recipe for products
# running on _ALL_ Raspberry Pi
# (all models all revisions).
#-------------------------------------------

# Raspberry Pi default Ethernet MAC range
# (for automatic probing on local subnet)
export BOARD_MAC_RANGE ?= b8:27:eb


# List of applications this platform needs. Packages
# will be built in the listed order (unless you
# explicitly define the dependencies).
PKGS_APPS += busybox/busybox-1.22.1
PKGS_APPS += dropbear/dropbear-2018.76
PKGS_APPS += f2fs-tools/f2fs-tools-1.6.0
PKGS_APPS += dosfstools/dosfstools-3.0.26
PKGS_APPS += autofs/autofs-5.1.3-archive/autofs-5.1.3
PKGS_APPS += z-lib/z-lib-1.2.8
PKGS_APPS += rpiburn/rpiburn-0.11


# List of application patches this platform needs.
linux-rpi-3.10.y.patches += drivers/mmc/host/sdhci-bcm2708.c
linux-rpi-3.10.y.patches += drivers/mmc/core/mmc.c
linux-rpi-3.10.y.patches += drivers/usb/host/dwc_otg/dwc_otg_cil.c
linux-rpi-4.4.y.patches += drivers/mmc/host/bcm2835-mmc.c
linux-rpi-4.4.y.patches += drivers/mmc/host/bcm2835-sdhost.c
linux-rpi-4.4.y.patches += drivers/char/hw_random/bcm2835-rng.c
linux-rpi-4.4.y.patches += drivers/clocksource/Makefile
linux-rpi-4.4.y.patches += drivers/usb/host/dwc_otg/dwc_otg_hcd_linux.c
linux-rpi-4.4.y.patches += drivers/usb/gadget/function/f_rndis.c
linux-rpi-4.4.y.patches += drivers/usb/gadget/function/f_ecm.c
linux-rpi-4.4.y.patches += drivers/usb/dwc2/core_intr.c
linux-rpi-4.4.y.patches += drivers/firmware/raspberrypi.c
linux-rpi-4.4.y.patches += arch/arm/Kconfig
linux-rpi-4.4.y.patches += arch/arm/Kconfig.debug
linux-rpi-4.4.y.patches += arch/arm/Makefile
linux-rpi-4.4.y.patches += arch/arm/mach-bcm2709/include/mach/platform.h
linux-rpi-4.4.y.patches += arch/arm/mach-bcm2709/include/mach/memory.h
linux-rpi-4.4.y.patches += arch/arm/mach-bcm2709/include/mach/uncompress.h
linux-rpi-4.4.y.patches += arch/arm/mach-bcm2709/bcm2709.c
linux-rpi-4.4.y.patches += arch/arm/mach-bcm2709/bcm2708.c
linux-rpi-4.4.y.patches += arch/arm/mach-bcm2709/bcm27xx.c
linux-rpi-4.4.y.patches += arch/arm/mach-bcm2709/bcm27xx.h
linux-rpi-4.4.y.patches += arch/arm/mach-bcm2709/Makefile
linux-rpi-4.4.y.patches += include/soc/bcm2835/raspberrypi-firmware.h
f2fs-tools-1.3.0.patches += Makefile
f2fs-tools-1.3.0.patches += mkfs/f2fs_format.c
f2fs-tools-1.3.0.patches += fsck/main.c
f2fs-tools-1.6.0.patches += Makefile
f2fs-tools-1.6.0.patches += mkfs/f2fs_format.c
f2fs-tools-1.6.0.patches += mkfs/f2fs_format_main.c
f2fs-tools-1.6.0.patches += fsck/mount.c
busybox-1.22.1.patches += networking/ntpd.c
busybox-1.22.1.patches += networking/udhcp/common.c
ftdi-lib-1.4.patches += Makefile
ftdi-lib-1.4.patches += Rules.mk
ftdi-lib-1.4.patches += src/Makefile
ftdi-lib-1.4.patches += src/ftdi_version_i.h
telldus-2.1.3-beta1.patches += Makefile
telldus-2.1.3-beta1.patches += Rules.mk
telldus-2.1.3-beta1.patches += telldus-core/Makefile
telldus-2.1.3-beta1.patches += telldus-core/common/Makefile
telldus-2.1.3-beta1.patches += telldus-core/common/EventHandler_unix.cpp
telldus-2.1.3-beta1.patches += telldus-core/service/Makefile
telldus-2.1.3-beta1.patches += telldus-core/service/service/config.h
telldus-2.1.3-beta1.patches += telldus-core/client/Makefile
telldus-2.1.3-beta1.patches += telldus-core/tdtool/Makefile
telldus-2.1.3-beta1.patches += telldus-core/tdadmin/Makefile
dropbear-2018.76.patches += localoptions.h
dropbear-2018.76.patches += ifndef_wrapper.sh


# Explicit package dependencies (optional, but
# nice to have for librarys)
dropbear/dropbear-2013.62: z-lib/z-lib-1.2.8
dropbear/dropbear-2018.76: z-lib/z-lib-1.2.8
debian/debian: busybox/busybox-1.22.1
dosfstools/dosfstools-3.0.26: busybox/busybox-1.22.1
ftdi-lib/ftdi-lib-1.4: confuse-lib/confuse-lib-2.7
ftdi-lib/ftdi-lib-1.4: usb-lib/usb-lib-1.0.20
telldus/telldus-2.1.3-beta1: ftdi-lib/ftdi-lib-1.4
telldus/telldus-2.1.3-beta1: confuse-lib/confuse-lib-2.7
telldus/telldus-2.1.3-beta1: usb-lib/usb-lib-1.0.20
privoxy/privoxy-3.0.26: z-lib/z-lib-1.2.8
wolfssl-lib/wolfssl-lib-3.15.5: z-lib/z-lib-1.2.8
openssl-lib/openssl-lib-1.1.1a: z-lib/z-lib-1.2.8
tinc/tinc-1.1pre17: openssl-lib/openssl-lib-1.1.1a
usb-compat-lib/usb-compat-lib-0.1.7: usb-lib/usb-lib-1.0.20
sispmctl/sispmctl-4.7: usb-compat-lib/usb-compat-lib-0.1.7


# List of utilities this platform needs
PKGS_UTILS += crosstool-ng/crosstool-ng-1.23.0.99.g8f8e131d-archive/crosstool-ng-1.23.0.99.g8f8e131d
PKGS_UTILS += glibc/glibc-2.22
PKGS_UTILS += mtools/mtools-4.0.18
PKGS_UTILS += insight/insight-7.4.50



#-----------------------------
# Get defines which tell what hw and
# sw we use and thus need (if any).
BOARD_DEPS += default
-include $(PATH_TOP)/platform/default/Rules.mk

