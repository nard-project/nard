
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2019 Ronny Nilsson


# -------------------------------------------------------------
# Global variables
pathSdCard="/dev/mmcblk0"



# -------------------------------------------------------------
# Disable hotpluging
hotplug_disable() {
	local pathHp hp

	pathHp="/proc/sys/kernel/hotplug"
	[ -r "$pathHp" ] || return 0

	read -t 5 -s hp <"$pathHp"
	while [ -n "$hp" ]; do
		flock -x /var/lock/hotplug echo >"$pathHp"
		read -t 5 -s hp <"$pathHp"
	done
}



# -------------------------------------------------------------
# Poor mans dynamic cpu offline. Raspberry Pi lack cpu hotplug
# support and power management. Instead we move all processes
# to cpu 0, which will make the remaining cores to idle and
# thus reduce power consumption.
# https://www.raspberrypi.org/forums/viewtopic.php?t=99372#p693896
become_uniprocessor() {
	local a pid

	[ -x "/usr/bin/taskset" ] || return 0

	for f in /proc/[0-9]*/status; do
		a=${f%/*}
		pid=${a##/proc/}
		[ -d "/proc/${pid}" ] && /usr/bin/taskset -p 1 $pid >/dev/null 2>&1		# Ignore errors, best effort
	done
}



# -------------------------------------------------------------
# Reset USB peripherals
reset_usb() {
	local f

	[ -d "/sys/devices/platform/soc" ] || return 0

	for f in /sys/devices/platform/soc/*usb/buspower; do
		[ -w "${f}" ] || continue
		echo 0 >"${f}"
	done
}



# -------------------------------------------------------------
# Test that the power supply has enough juice by loading the
# system to the max for a short period while we simultaneously
# monitor the brownout sensor. Sync disks first due to a power
# supply test may worst case cause a crash.
psu_test() {
	if [ -x "/usr/sbin/rpiburn" ]; then
		sync && sleep 1

		if ! "/usr/sbin/rpiburn"; then
			echo "Error; bad power supply"
			return 1
		fi
	fi

	return 0
}



# -------------------------------------------------------------
# Write out all SD card disk buffers and clear the page cache
flush_sdcard() {
	local B

	/bin/sync
	echo 1 >/proc/sys/vm/drop_caches

	for B in ${pathSdCard}*; do
		if [ -b "$B" ]; then
			/bin/fsync "$B"
			/sbin/blockdev --flushbufs "$B"
		fi
	done

	/bin/sleep 1

	return 0
}



# -------------------------------------------------------------
# Returns true if there is a SD card device.
has_Sd_card() {
	test -b "$pathSdCard"
}



# -------------------------------------------------------------
# Returns true if the SD card MBR is in native Nard format.
# (As an alternative it can be user customized.)
has_nard_mbr() {
	has_Sd_card && dd if="$pathSdCard" bs=1 count=4 \
		skip=440 2>/dev/null | strings -n 2 | grep -qi "NARD"
}



# -------------------------------------------------------------
# Slow down communication with the SD card to increase
# robustness. To be used before writing lots of data.
# Performs dynamic lookup of which MMC device is the card.
slowdown_Sd_card() {
	local mmcDevPath mmcDevice D mmcClock rate

	mmcDevPath="/sys/bus/mmc/drivers/mmcblk"
	mmcClock=""

	if [ -d "$mmcDevPath" ]; then
		mmcDevice=$(cd "${mmcDevPath}" && echo mmc*)

		for D in /sys/kernel/debug/mmc*/*; do
			[ "${D##*/}" = "$mmcDevice" -a -f "${D%/*}/clock" ] && \
				mmcClock="${D%/*}/clock"
		done

		if [ -f "$mmcClock" ]; then
			read -t 5 -s rate <"$mmcClock"
			[ $rate -gt 16000000 ] && echo 16000000 >"$mmcClock"
			usleep 100000
		fi
	fi
}



# -------------------------------------------------------------
# Return a string describing Nard version in use.
nard_ver() {
	local line

	[ -e "/etc/release" ] || return
	read -t 5 -s line <"/etc/release"
	echo -n "${line:0:63}"
}



# -------------------------------------------------------------
# Return a unique system identifier string
# which is the same across reboots.
sys_id() {
	local sid buf line

	buf=""

	if [ ! -e "/proc/cpuinfo" ]; then
		echo -n "0"
		return
	fi

	while read -t 5 -s line; do
		case "$line" in
			[Hh]ardware*|[Rr]evision*|[Ss]erial*|[Mm]odel*)
				buf="${buf}${line}"$'\n'
				;;
		esac
	done <"/proc/cpuinfo"

	if [ -z "$buf" ]; then
		echo -n "0"
		return
	else
		sid=$(echo -n "$buf" | md5sum)
		[ -n "$sid" ] || sid="0"
		echo "${sid:0:15}"
	fi
}



# -------------------------------------------------------------
# Return the system serial number.
sys_serial() {
	local line

	if [ ! -e "/proc/cpuinfo" ]; then
		echo -n "0"
		return 1
	fi

	while read -t 5 -s line; do
		case "$line" in
			[Ss]erial*:\ 00000000000000*)
				echo -n ${line##*:\ 00000000000000}
				return 0
				;;
			[Ss]erial*:\ 000000000000*)
				echo -n ${line##*:\ 000000000000}
				return 0
				;;
			[Ss]erial*:\ 0000000000*)
				echo -n ${line##*:\ 0000000000}
				return 0
				;;
			[Ss]erial*:\ 00000000*)
				echo -n ${line##*:\ 00000000}
				return 0
				;;
			[Ss]erial*:\ 000000)
				echo -n ${line##*:\ 000000}
				return 0
				;;
			[Ss]erial*:\ 0000)
				echo -n ${line##*:\ 0000}
				return 0
				;;
			[Ss]erial*:\ 00)
				echo -n ${line##*:\ 00}
				return 0
				;;
			[Ss]erial*:\ )
				echo -n ${line##*:\ }
				return 0
				;;
		esac
	done <"/proc/cpuinfo"

	echo -n "0"
	return 1
}



# -------------------------------------------------------------
# Renice current shell process (incl. subshells) to absolute
# value $1. The standard "renice" command can't handle subshells.
renice_this() {
	local line pid

	read -t 5 -s line </proc/self/stat
	pid="${line%%[[:blank:]]*}"
	[ -n "$pid" ] || return
	[ -d "/proc/${pid}" ] || return
	renice $1 -p $pid
}



# -------------------------------------------------------------
# Read user configurations previously copied from SD-card
# FAT partition or downloaded from PXE net boot server.
source_settings() {
	local F line errmsg

	[ -d "/etc/settings" ] || return 1
	echo -n >"/tmp/userconfig-$$"

	# Concatenate all user configurations, in alphabetical order,
	# which are in plain text format. Skip binary files, SSH keys,
	# shell scripts and all subdirectorys.
	for F in $(find -L /etc/settings -type f -print0 | sort -z | \
			xargs -0r grep -EL [$'\x01'-$'\x07']"|^ssh-(rsa|dsa)|^#!/"); do
		dos2unix "$F"
		name="${F##*/}"
		[ "${name:0:3}" = "rc." ] && continue									# Skip: rc.local
		[ "${name##*.}" = "sh" ] && continue									# Skip: file.sh
		[ -z "${F##/*/*/*/*}" ] && continue										# Skip subdirs
		grep -Eo "^[[:alnum:]]+[^=#]+=[^=#]*" "$F" >>"/tmp/userconfig-$$"
	done

	# Source all joined configurations.
	errmsg=$(sh -e "/tmp/userconfig-$$" 2>&1)
	[ -n "$errmsg" ] && \
		logger -s -p daemon.err -t nard-init "Error parsing settings! $errmsg"
	source "/tmp/userconfig-$$"

	# Translate upper case to lower case and source that as
	# well due to the user might come from the Windows world.
	tr "[[:upper:]]" "[[:lower:]]" <"/tmp/userconfig-$$" >"/tmp/userconfig2-$$"
	errmsg=$(sh -e "/tmp/userconfig2-$$" 2>&1)
	[ -n "$errmsg" ] && \
		logger -s -p daemon.err -t nard-init "Error parsing settings! $errmsg"
	source "/tmp/userconfig2-$$"

	rm -f "/tmp/userconfig2-$$" "/tmp/userconfig-$$"
}



# -------------------------------------------------------------
# Set system hostname from user conf.
set_sys_hostname() {
	local serial

	if [ -n "$1" ]; then
		hostname="$1"
	elif [ -z "$hostname" ]; then
		if [ -s "/etc/hostname" ]; then
			read -t 5 -s hostname </etc/hostname
		else
			# Create a dynamic hostname, dependant
			# of the cpu serial number (which the
			# ethernet mac also uses).
			hostname="$PRODUCT"
			serial=$(sys_serial)
			[ -n "$serial" ] && hostname="${PRODUCT}-${serial}"
		fi
	fi
	
	if [ -n "$hostname" ]; then
		echo "Setting hostname ${hostname}..."
		hostname "$hostname"
		export hostname
	fi
}



# -------------------------------------------------------------
# Get system hostname from the kernel.
get_sys_hostname() {
	read -t 5 hostname <"/proc/sys/kernel/hostname"
	echo -n "$hostname"
}



# -------------------------------------------------------------
# Add the optional device node ID to the issue file which is
# read by the telnet and getty daemons. Users connected to the
# local Ethernet subnet may then query ALL devices at once by
# simply connecting to the telnet port. No need to login.
set_sys_issue() {
	local line

	# Is the issue file already fixed up? Then do nothing.
	while read -t 5 -s line; do
		case "$line" in
			NodeId*)
				return 0
				;;
		esac
	done <"/etc/issue"


	[ -z "$nodeid" ] && nodeid="0"
	echo "NodeId: $nodeid" >>/etc/issue
	echo "NodeId: $nodeid" >>/etc/issue.net
	[ -n "$nodename" ] && echo "NodeName: $nodename" >>/etc/issue
	[ -n "$nodename" ] && echo "NodeName: $nodename" >>/etc/issue.net

}


