
# Network related shell script function library.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2019 Ronny Nilsson



# -------------------------------------------------------------
# Print the IPv4 address of a network interface.
ip_of_nic() {
	[ -e "/sys/class/net/${1}" ] || return 1

	ip -family inet address show $1 |											\
		grep -Eo "^[[:space:]]*inet[^[:alpha:]]+" |								\
		grep -Eo "([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}" |					\
		head -n 1
}



# -------------------------------------------------------------
# Get user configuration from SD-card
# for a particular interface.
get_user_conf_for_nic() {
	local a b var

	[ -z "${1}" ] && return
	[ -s "/etc/settings/hostname" ] && source "/etc/settings/hostname"
	[ -z "$hostname" ] && read -t 5 -s hostname <"/proc/sys/kernel/hostname"
	[ -s "/etc/settings/network" ] && source "/etc/settings/network"

	# Dynamically create environment variables according to a list.
	for var in mac proto ip0 mask0 mtu defgw dns1 dns2 ssid psk freq; do
		a="$(eval echo -n \$${1}${var})"
		b="$(eval echo -n \$${1}_${var})"
		eval iface${var}="$(echo -n ${a:-${b}})"
		#echo "conf ${1} iface${var}=$(eval echo -n \$iface${var})" >>$PATH_LOG
	done

	[ -z "$ifaceproto" ] && ifaceproto="dynamic"
}



# -------------------------------------------------------------
# Return true if network interface $1 is of regular
# type (not loopback or point to point).
is_nic_regular() {
	local iface flags

	iface="$1"
	[ -n "$iface" ] || return 1

	[ -e "/sys/class/net/${iface}/flags" ] || return 1
	read -t 5 -s flags <"/sys/class/net/${iface}/flags"

	[ $((flags & 8)) -gt 0 ] && return 1
	[ $((flags & 16)) -gt 0 ] && return 1
	[ $((flags & 2)) -eq 0 ] && return 1

	return 0
}

