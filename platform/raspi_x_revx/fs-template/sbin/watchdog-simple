#!/bin/ash

# Simple watchdog kicker which continously runs in the background.
# Features:
# - Creates new processes regularly -> test kernel process table not full
# - Checks writing temporary files works
# - Flashes a heartbeat LED for operator inspection
# - Runs with a low priority which may catch some runaway processes
# - Cooperates with the system shutdown script to prevent ahead of time resets
#
# Info:
# - The LED can be disabled by adding "no-hearbeat-led" to /boot/cmdline.txt
# (the kernel commandline).
# - The LED frequency might become slower when the cpu is loaded.
# - This script may be disabled by adding "no-watchdog" to /boot/cmdline.txt.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2017 Ronny Nilsson



# -------------------------------------------------------------
init() {
	wdogFd=$(($(cd /proc/$$/fd; ls -1 | sort -g | tail -n 1) + 2))				# Find a free file descriptor
	wdogPath=/dev/watchdog
	ledPath=/dev/led-act														# Created by boot scripts
	ledUse=1
	ledVal=0
	grep -q "no-hearbeat-led" /proc/cmdline && ledUse=0
	doExit=0

	[ -e "$wdogPath" ] || exit 0
	grep -q "no-watchdog" /proc/cmdline && exit 0

	# By giving our self a low priority we
	# might catch some runaway processes.
	renice 9 -p $$

	# Make our self more killable in
	# in a low memory situation.
	echo 650 >/proc/$$/oom_score_adj

	trap stop_wdog sighup
	trap stop_wdog sigint
	trap stop_wdog sigquit
	trap stop_wdog sigterm
}


# -------------------------------------------------------------
start_wdog() {
	eval "exec $wdogFd>$wdogPath"

	if [ $(readlink /proc/$$/fd/$wdogFd) != "$wdogPath" ]; then
		logger "Can't open $wdogPath"
		exit 1
	fi
}


# -------------------------------------------------------------
# When the system is about to reboot the /etc/rc.syshalt
# script will send us a signal which make us stop the
# watchdog. This must be done due to BCM2708 watchdog driver
# has a quite low timeout. If we didn't stop it we might
# become reseted ahead of time. To prevent a hanging shutdown
# the shutdown script has an internal watchdog.
stop_wdog() {
	[ $doExit -eq 1 ] && return
	doExit=1
	echo -n "V" >&$wdogFd
	eval "exec $wdogFd>&-"

	# Permanent LED on during system shut down
	[ -L "$ledPath" -a $ledUse -eq 1 ] && echo 1 >"$ledPath"
}



# -------------------------------------------------------------
kick_wdog() {
	[ $doExit -eq 1 ] && return
	echo >&$wdogFd || doExit=1
}



# -------------------------------------------------------------
# Flash the board ACT LED if user hasn't disabled it
flash_led() {
	[ -L "$ledPath" -a $ledUse -eq 1 ] || return

	ledVal=$(((ledVal + 1) & 1))
	echo $ledVal >"$ledPath"
}


# -------------------------------------------------------------
# Create a temporary file for checking that
# the root filesystem is working.
test_fs() {
	if [ -s /var/tmp/wdog-simple.$$ ]; then
		# Truncate to empty file with Ash internal command
		echo -n "" >/var/tmp/wdog-simple.$$ || doExit=1
	else
		echo "watchdog $RANDOM" >>/var/tmp/wdog-simple.$$ || doExit=1
	fi
}



# =============================================================
init
start_wdog
kick_wdog

while [ $doExit -eq 0 ] && sleep 1; do
	test_fs
	kick_wdog
	flash_led
done

# Wait for stop_wdog() to run in 
# case of friendly system shutdown.
echo 850 >/proc/$$/oom_score_adj
until grep -qiE "SigPnd:.[[:xdigit:]]+0000$" /proc/$$/status; do
	sleep 0.2
done

exit 0

