#!/bin/ash

# Script which will aid in upgrading to a new version of the
# operating system and applications. To be used in cooperation
# with Nard Linux automatic upgrade tools.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2019 Ronny Nilsson



# -------------------------------------------------------------
# Empty stubs for avoiding errors. Is defined in functions.sh.
become_uniprocessor() { return 0; }
psu_test() { return 0; }
has_Sd_card() { return 0; }
has_nard_mbr() { return 0; }



# -------------------------------------------------------------
# Write out all SD card disk buffers and clear the page cache
flush_sdcard() {
	sync
	fsync "/dev/mmcblk0"
	blockdev --flushbufs "/dev/mmcblk0"
	echo 3 >/proc/sys/vm/drop_caches
	sleep 1
	echo 0 >/proc/sys/vm/drop_caches
	return 0
}


# -------------------------------------------------------------
init() {
	local A

	PATH_FS=initramfs.cpio.gz
	PATH_KERNEL=zImage
	PATH_ARCHIVE=sdcard.tar.gz
	export PATH_UPLOAD=/tmp
	export PATH_BOOT=/boot
	export PATH_STAGING="${PATH_BOOT}/firmware-staging"
	export PATH_PLUGINS="${PATH_BOOT}/upgrade-plugins.d"
	export PATH="${PATH}:/sbin:/usr/sbin"

	# Lower our priority
	renice -n 8 $$

	# Are we running as the super user?
	test $(id -u) -eq 0 || exit 0

	# Do we have all files received?
	test -e "${PATH_UPLOAD}" || exit 0
	test -s "${PATH_UPLOAD}/${PATH_ARCHIVE}" || exit 0

	# Check that uploaded files is owned by root
	# and seems of a valid size.
	A=$(find "${PATH_UPLOAD}/${PATH_ARCHIVE}" \
		-user 0 -group 0 -type f -size +4096k -maxdepth 0)
	test -z "$A" && exit 0

	# Pull library functions
	[ -r "/lib/nard/functions.sh" ] && source "/lib/nard/functions.sh"
}


# -------------------------------------------------------------
cleanup() {
	rm -rf "${PATH_UPLOAD}/${PATH_ARCHIVE}" \
		"${PATH_UPLOAD}/orig-settings.md5sum" \
		"${PATH_STAGING}"
}


# -------------------------------------------------------------
msg() {
	if [ -n "$1" ]; then
		logger -s -p warn "$1"
		echo "$1" >>"/var/log/upgrade-os.log"
	fi
}


# -------------------------------------------------------------
msg_corrupt_and_die() {
	[ -n "$1" ] && msg "$1"
	msg "Warning SD card might be corrupt, do NOT reboot until you've"
	msg "fixed the problem manually! Further help here:"
	msg "http://www.arbetsmyra.dyndns.org/nard/help.html#reimage"
	cleanup 2>/dev/null
	exit 1
}


# -------------------------------------------------------------
# Verify md5sum of files with a preceding check of
# the md5sum input file. Is it a valide MD5 file?
# In corrupt filesystems it will be random binary.
check_md5sum() {
	test -s "$1" || return
	head -c 1024 "$1" | grep -qE "^[[:xdigit:]]{32}[[:space:]]" || return
	nice -n 12 md5sum -sc "$1"
}


# =============================================================
init


# Peek into the archive and check that we
# have the expected files (before we actually
# extract them). Don't do a cleanup if not.
if ! tar -xOf "${PATH_UPLOAD}/${PATH_ARCHIVE}" \
		"${PATH_FS}.md5sum" 2>/dev/null | \
		grep -qE "^[[:xdigit:]]+"; then
	exit 0
fi


# OK we have a good looking archive candidate in $PATH_UPLOAD.
# Prepare the system for OS upgrade. Begin a new log.
rm -f "/var/log/upgrade-os.log"
msg "Extracting archive and preparing..."


# Slow down communication with the SD card to increase
# robustness. We will write lots of data soon.
if type slowdown_Sd_card >/dev/null; then
	slowdown_Sd_card
elif [ -f "/sys/kernel/debug/mmc0/clock" ]; then								# Remove once users has upgraded.
	read -t 5 -s rate <"/sys/kernel/debug/mmc0/clock"
	[ $rate -gt 16000000 ] && echo 16000000 >"/sys/kernel/debug/mmc0/clock"
	usleep 100000
fi


# Check there is a SD card device file
if ! has_Sd_card; then
	msg "Error; no SD card available!"
	cleanup
	exit 1
fi


# Test that the power supply has enough juice by loading
# the system to the max for a short period while we
# simultaneously monitor the brownout sensor.
if ! psu_test >/dev/null; then
	msg "Error; bad power supply or cable"
	cleanup
	exit 1
fi


# Poor mans dynamic cpu offline. Raspberry Pi lack cpu hotplug
# support and power management. Instead we move all processes
# to cpu 0, which will make the remaining cores to idle and thus
# reduce board power consumption. Hopefully this will leave
# enough power left for the SD card which will need to work hard.
become_uniprocessor


if [ -e "${PATH_BOOT}" ]; then
	# Check SD-card filesystem for issues before commencing intence writes
	msg "Forcing unmount of all SD card partitions..."
	killall -q -USR1 automount && sleep 1
	mountpoint -q "${PATH_BOOT}" && umount "${PATH_BOOT}"
	flush_sdcard
	if ! mountpoint -q "${PATH_BOOT}"; then
		msg "Prerequisite check for possible SD-card FS issues..."
		msg "$(fsck -t vfat -- -a -c 850 2>&1 | \
			while read l; do echo \ \ \ \ $l; done)"							# Indent fsck output with spaces
		flush_sdcard
	fi

	# Check that we can mount SD-card, where firmware should be stored
	ls -L "${PATH_BOOT}" >/dev/null												# Trigger the automounter	
	if ! mountpoint -q $(realpath ${PATH_BOOT}); then
		msg_corrupt_and_die "Error mounting $(realpath ${PATH_BOOT})"
	fi
else
	msg "Missing ${PATH_BOOT}, nowhere to store uploads"
	cleanup
	exit 1
fi

# Ensure SD-card is asynchronous, we might have come
# here from a previous failed upgrade.
mount $(realpath ${PATH_BOOT}) -o remount,rw,async,dirsync,nosuid,nodev,noatime,nodiratime,norelatime,noexec,flush,quiet,showexec,discard


# Create checksums of the device unique settings in
# the SD card for later integrity verification.
find -H "${PATH_BOOT}/settings" -xdev -type f -print0 | \
	xargs -r0 md5sum >"${PATH_UPLOAD}/orig-settings.md5sum"


# Remove any remains from fsck and prev failed upgrade.
rm -rf /boot/*.REC "${PATH_STAGING}"
mkdir -p "${PATH_STAGING}"


# Check if the SD card har a Nard native MBR before
# we start massive writes and it possibly corrupts.
has_nard_mbr && saved_has_mbr=1 || saved_has_mbr=0


# Extract the tarball with new kernel and fs
# to a temporary dir in the SD-card.
if ! msg "$(timeout -t 1200 pipe_progress <"${PATH_UPLOAD}/${PATH_ARCHIVE}" | \
		tar -C "${PATH_STAGING}" -xz 2>&1 | \
		while read l; do echo \ \ \ \ $l; done)"; then							# Indent tar output with spaces
	msg_corrupt_and_die "Error; couldn't extract ${PATH_UPLOAD}/${PATH_ARCHIVE}"
fi


# Do we have a minimum set of files?
if test ! -s "${PATH_STAGING}/${PATH_FS}.md5sum" || \
		test ! -s "${PATH_STAGING}/${PATH_KERNEL}.md5sum" || \
		test ! -s "${PATH_STAGING}/${PATH_FS}" || \
		test ! -s "${PATH_STAGING}/${PATH_KERNEL}"; then
	msg "Missing necessary files for upgrade"
	cleanup
	exit 1
fi


# Check that uploaded files seems of a valid size
A=$(find "${PATH_STAGING}/${PATH_KERNEL}" \
	-type f -size +2048k -maxdepth 0)
if test -z "$A"; then
	msg_corrupt_and_die "Error; invalid size of ${PATH_KERNEL}"
fi

A=$(find "${PATH_STAGING}/${PATH_FS}" \
	-type f -size +2048k -maxdepth 0)
if test -z "$A"; then
	msg_corrupt_and_die "Error; invalid size of ${PATH_FS}"
fi


# Flush and forget all disk buffers. We want to read back
# what is really in there when we verify checksums. (Data
# might have changed behind our back due SD card corruption.)
flush_sdcard


# Check that uploaded MD5 checksums are correct.
if (test -s "${PATH_STAGING}/upgrade.md5sum" && \
		! (cd "${PATH_STAGING}" && check_md5sum "upgrade.md5sum")) || \
		! (cd "${PATH_STAGING}" && check_md5sum "${PATH_KERNEL}.md5sum") || \
		! (cd "${PATH_STAGING}" && check_md5sum "${PATH_FS}.md5sum"); then
	# Delete uploaded files on error
	msg "Error; invalid checksum of uploaded files"
	msg_corrupt_and_die
fi


# Check if we're already running what we've got uploaded.
if cmp -s "${PATH_BOOT}/${PATH_KERNEL}" "${PATH_STAGING}/${PATH_KERNEL}" && \
		cmp -s "${PATH_BOOT}/${PATH_FS}" "${PATH_STAGING}/${PATH_FS}"; then
	msg "Uploaded files are the same we already have, skipping upgrade"
	cleanup
	exit 1
fi


msg "Upgrading system..."

sync
mount $(realpath ${PATH_BOOT}) -o remount,rw,sync,dirsync,nosuid,nodev,noatime,nodiratime,norelatime,noexec,flush,quiet,showexec,discard


# Remove any old optional files left behind which
# could otherwise confuse the upgrade process.
rm -rf "$PATH_PLUGINS" "${PATH_BOOT}/upgrade.md5sum"

# Create an equal directory tree structure in /boot as in tarball
dirs=$(cd "${PATH_STAGING}" && find * -type d)
if test $? -ne 0 -o -z "$dirs" || \
		! (cd "${PATH_BOOT}" && timeout -t 180 mkdir -p $dirs); then
	msg_corrupt_and_die "Error creating tree copy of ${PATH_STAGING}"
fi


# Rename files (only) for trying to keep the file inconsitency
# as short as possible (FAT has no journaling). Overwrite
# everything except the unique device settings.


(cd "${PATH_STAGING}" &&
	for f in $(find * -type f ! -path "settings/*"); do
		if ! timeout -t 60 mv -f "$f" "${PATH_BOOT}/${f}"; then
			msg_corrupt_and_die "Error renaming firmware-file $f"
		fi

		# Inform the timeout background timer that "mv"
		# has finished, to avoid a herd of sleepyheads.
		pgrep -f "^timeout -t [[:digit:]]+ mv -f $f ${PATH_BOOT}/${f}" | \
			xargs -r kill -HUP
	done
)


(cd "${PATH_STAGING}" &&
	for f in $(find * -type f -path "settings/*"); do
		if ! timeout -t 60 mv -n "$f" "${PATH_BOOT}/${f}"; then
			msg_corrupt_and_die "Error renaming settings-file $f"
		fi

		# Inform the timeout background timer that "mv"
		# has finished, to avoid a herd of sleepyheads.
		pgrep -f "^timeout -t [[:digit:]]+ mv -n $f ${PATH_BOOT}/${f}" | \
			xargs -r kill -HUP
	done
)


# Check again that uploaded MD5 checksums are correct.
if ! flush_sdcard || (test -s "${PATH_BOOT}/upgrade.md5sum" && \
		! (cd "${PATH_BOOT}" && check_md5sum "upgrade.md5sum")); then
	msg_corrupt_and_die "Error verifying upgrade-files in ${PATH_BOOT}"
fi


# Compare the current device unique settings with
# the checksums we created. Are they still valid?
if test -s "${PATH_UPLOAD}/orig-settings.md5sum" && \
		! check_md5sum "${PATH_UPLOAD}/orig-settings.md5sum"; then
	msg_corrupt_and_die "Error verifying SD card settings in ${PATH_BOOT}/settings"
fi


# Check that the MBR is still valid.
if test $saved_has_mbr -eq 1 && ! has_nard_mbr; then
	msg_corrupt_and_die "Error verifying SD card master boot record"
fi


# Are there shell scripts bundled in the upgrade tarball which
# extend this upgrade program? then run them now. We can't
# use "run-parts" due to the filesystem is mounted noexec.
if test -d "$PATH_PLUGINS"; then
	plugins=$(/bin/ls -1 "${PATH_PLUGINS}/"*)
	if test -n "$plugins"; then
		for scr in $plugins; do
			if test -e "${scr}" && ! timeout -t 900 ash "${scr}"; then
				msg "Error in plug-in ${scr}, upgrade aborted!"
				cleanup
				exit 1
			fi
		done
	fi
fi


cleanup
sync


msg "Upgrade of system done, issuing a reboot"									# Don't modify this string!
delay=8
tty -s </proc/self/fd/1 && delay=1												# Limit delay if a user is watching stdout
command setsid /sbin/reboot -d $delay </dev/null >/dev/null 2>&1
exit 0

