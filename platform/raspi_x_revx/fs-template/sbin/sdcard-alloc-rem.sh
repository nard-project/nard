#!/bin/ash

# Script for allocating remaining free space of the SD-card.
# If the SD-card has _only_one_ partition of file system type
# FAT32 and there is at least 512 MB of free unallocated
# space at the end, we will create a new partition for making use of
# the entire SD-card. Script is to be run early at system
# boot where it will enable users to utilize all SD-cards,
# regardless of what size it has. Yet a single static image
# can be exported from Nard SDK that will also work with
# all SD-cards.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2019 Ronny Nilsson


# -------------------------------------------------------------
init() {
	DISK_DEV=mmcblk0
	PATH_DISK=/sys/block
	PATH_TMPMBR=/tmp/sdcard.mbr
	PATH_MOUNT=/mnt/sdcard/user
	PATH_LOFILE="/var/spool/volatile-storage-user.ext2"
	DISK_USED=0
	DISK_PARTS=0
	DISK_SIZE=0
	DISK_SECTOR=0
	DISK_START=0
	DISK_REM=0
	DISK_TOT=""
	OPT=""
		
	# Are we running as the super user?
	test $(id -u) -eq 0 || exit 0
	
	# Has the user configured us to
	# not do auto SD-card allocation?
	[ -e /proc/cmdline ] && grep -q "no-sdcard-alloc" /proc/cmdline && exit 0

	# Pull library functions
	[ -r "/lib/nard/functions.sh" ] && source "/lib/nard/functions.sh"
}



# -------------------------------------------------------------
# Patch the SD card MBR to include remaing part of free space.
create_persistent_storage_sdcard() {
	local P FS_TYPE NEW_SIZE B0 B1 B2 B3 TIMEOUT

	# Is the SD-card availible?
	test -b "/dev/${DISK_DEV}" || return 1
	test -b "/dev/${DISK_DEV}p1" || return 1
	test -d "${PATH_DISK}/${DISK_DEV}" || return 1
	test -f "${PATH_DISK}/${DISK_DEV}/size" || return 1
	
	
	# Calculate currently allocated space in the SD-card
	read -t 5 -s DISK_SIZE <${PATH_DISK}/${DISK_DEV}/size
	DISK_SECTOR=$(blockdev --getss /dev/${DISK_DEV})
	
	for P in ${PATH_DISK}/${DISK_DEV}/${DISK_DEV}p[1-9]; do
		test -d "$P" || return 1
		read -t 5 -s DISK_USED <${P}/size
		read -t 5 -s DISK_START <${P}/start
		DISK_PARTS=$((DISK_PARTS + 1))
		DISK_TOT="${DISK_TOT}${DISK_START} ${DISK_USED}"$'\n'
	done
	DISK_START=$(echo -n "$DISK_TOT" | sort -g | tail -n 1 | cut -d " " -f 1)
	DISK_USED=$(echo -n "$DISK_TOT" | sort -g | tail -n 1 | cut -d " " -f 2)
	DISK_REM=$(((DISK_SIZE - DISK_START - DISK_USED) * DISK_SECTOR))
	
	# Some sanity checks of data
	test $DISK_SIZE -gt 200 || return 1
	test $DISK_SECTOR -gt 16 || return 1
	test $DISK_USED -gt 70 || return 1
	test $DISK_PARTS -ne 2 && return 1
	test $DISK_START -ge 512 || return 1
	test $DISK_REM -ge 0 || return 1
	test $((DISK_START * DISK_SECTOR)) -gt 10485760 || return 1
	
	# Is there less than 512 MB of free space (not belonging
	# to any partition) after the last geometrical partition?
	# Then the card has been fully allocated already; nothing
	# more to do.
	test $DISK_REM -le 536870912 && return 0
	
	# Test that the power supply has enough juice (prevent
	# filesystem corruption).
	psu_test || return 1


	# Copy the master boot record from the SD-card
	dd if="/dev/${DISK_DEV}" of="$PATH_TMPMBR" bs=512 count=1 2>/dev/null
	
	
	# Is the MBR created by Nard Linux SDK?
	if ! has_nard_mbr; then
		rm -f "$PATH_TMPMBR"
		return 2
	fi
	
	
	# Is the first existing partition of filesystem type
	# FAT32? (Nard generated images use this type).
	FS_TYPE=$(dd if="$PATH_TMPMBR" bs=1 skip=450 count=1 2>/dev/null | \
		tr "\000\013\014\203" "0BCL")
	if ! test "$FS_TYPE" = "C"; then
		rm -f "$PATH_TMPMBR"
		return 2
	fi
	
	
	# Is the second existing partition of filesystem type
	# "Linux"? (Nard generated images use this type).
	FS_TYPE=$(dd if="$PATH_TMPMBR" bs=1 skip=466 count=1 2>/dev/null | \
		tr "\000\013\014\203" "0BCL")
	if ! test "$FS_TYPE" = "L"; then
		rm -f "$PATH_TMPMBR"
		return 2
	fi
	
	
	# Ensure the disk is unmounted
	# before we modify the MBR.
	if grep -q ${DISK_DEV} /proc/mounts; then
		echo "The SD-card $DISK_DEV is in use! Aborting auto-allocation of free space."
		rm -f "$PATH_TMPMBR"
		return 3
	fi
	
	
	# Calculate the remainging size we will allocate, but
	# round it down to a multiple of the geometrical sector
	# size used by Nard. If it becomes less than 768 MB
	# something is likely wrong, then back off.
	NEW_SIZE=$((((DISK_SIZE - DISK_START - 1) / 63) * 63))
	if test $((NEW_SIZE * DISK_SECTOR)) -le 805306368 -o \
			$NEW_SIZE -le $DISK_USED -o \
			$((DISK_START + NEW_SIZE)) -ge $DISK_SIZE; then
		rm -f "$PATH_TMPMBR"
		return 4
	fi
	
	echo "There is more than 512 MB of free space in the SD-card /dev/$DISK_DEV"
	echo -n "  Will automatically set the last partition to new size "
	echo "$((NEW_SIZE * DISK_SECTOR / (1024 * 1024))) MB..."
	
	
	# Generate binary data of the new partition 
	# table in little endian format.
	rm -f "${PATH_TMPMBR}.2"
	dd if="$PATH_TMPMBR" of="${PATH_TMPMBR}.2" bs=1 count=474 2>/dev/null
	B0=$(dc 16 o $NEW_SIZE 255 and p)
	B1=$(dc 16 o $NEW_SIZE 256 / 255 and p)
	B2=$(dc 16 o $NEW_SIZE 65536 / 255 and p)
	B3=$(dc 16 o $NEW_SIZE 16777216 / 255 and p)
	printf "\x${B0}\x${B1}\x${B2}\x${B3}" >>"${PATH_TMPMBR}.2"
	dd if="$PATH_TMPMBR" of="${PATH_TMPMBR}.2" bs=1 count=34 \
		skip=478 seek=478 2>/dev/null
	
	# Overwrite the existing SD-card MBR with our new one
	if cat "${PATH_TMPMBR}.2" >"/dev/${DISK_DEV}"; then
		echo "  Wrote new MBR to SD-card, rereading partitiontable..."
		blockdev --flushbufs "/dev/${DISK_DEV}"
		blockdev --rereadpt "/dev/${DISK_DEV}"
	else
		echo "  Failed writing new MBR to SD-card!"
		rm -f "$PATH_TMPMBR" "${PATH_TMPMBR}.2"
		return 5
	fi
	
	# Remove temp files
	rm -f "$PATH_TMPMBR" "${PATH_TMPMBR}.2"
	
	# Wait for kernel to re-read the partition
	# table and create new block devices.
	TIMEOUT=10
	until test -b "/dev/${DISK_DEV}p2" -o $TIMEOUT -eq 0; do
		sleep 1
		TIMEOUT=$((TIMEOUT - 1))
	done
	
	if ! test -b /dev/${DISK_DEV}p2; then
		echo "  Creation of new parition failed!"
		return 6
	fi
	
	echo "  Creating new filesystem..."
	if ! mkfs.f2fs -l user -t 1 -o 6 "/dev/${DISK_DEV}p2"; then
		echo "  Creation of filesystem failed!"
		return 7
	fi

	# Mount the new filesystem. We expect it
	# to already have an entry in /etc/fstab.
	if test -d "$PATH_MOUNT"; then
		if ! mount "$PATH_MOUNT" || ! mountpoint -q "$PATH_MOUNT"; then
			echo "  Couldn't mount the new fs!"
			return 8
		fi

		if ! fstrim -v "$PATH_MOUNT"; then
			echo "  Couldn't trim the new fs!"
			return 9
		fi
	fi
}



# -------------------------------------------------------------
# If there is no SD card for persistent storage we
# create a volatile equivalent for emulation via a
# file loop mount.
create_volatile_storage_sdcard() {
	[ -s "$PATH_LOFILE" ] || {
		dd if=/dev/zero of="$PATH_LOFILE" bs=1k seek=20480 count=0 2>/dev/null
		mkfs.ext2 -m 0 -L volatilesdcard -F "$PATH_LOFILE" >/dev/null
	}

	mount -t ext2 "$PATH_LOFILE" "${PATH_MOUNT}" -o loop

	# Ensure there is a ext2 filechecker due to
	# automount will call it for dynamic mounting.
	[ -e "/sbin/fsck.ext2" ] || {
		echo "#!/bin/true" >/sbin/fsck.ext2
		chmod a+x /sbin/fsck.ext2
	}
}



# -------------------------------------------------------------
# Create a directory structure in the newly created filesystem.
populate_persistent_storage() {
	local USER GRP

	# Create a readme info for users of how
	# to use the SD card second partition.
	if test ! -s "${PATH_MOUNT}/Readme.txt"; then
		cat >"${PATH_MOUNT}/Readme.txt" <<-EOF
	
			This is the root directory of the SD card non-volatile storage.
			Create your custom application directory here by adding the line
			below to your file "platform/<product>/fs-template/etc/rc.<product>"
			(substitute <product> with the name of your custom product).
		
			[ -d "/sdcard/my-dir" ] || install -m 0755 -o root -g root -d "/sdcard/my-dir"
		
		EOF
	fi
	
	# Create home directorys for all static users
	test -d "${PATH_MOUNT}/home" || \
		install -m 0755 -o root -g root -d "${PATH_MOUNT}/home"
	
	for USER in $(grep -E ":.*/home/.*:" /etc/passwd | grep -vE "^volatile" | \
			cut -d : -f 1); do
		GRP=root
		grep -qE "^${USER}:" /etc/group && GRP=$USER
		test -d "/home/${USER}" || \
			install -m 2775 -o ${USER} -g ${GRP} -d "/home/${USER}"
	done

	# Symlink the volatile user to the ramdisk.
	test -e "/home/volatile" || ln -s "/var/home/volatile" "/home"
}



# =============================================================
init

create_persistent_storage_sdcard || create_volatile_storage_sdcard

if mountpoint -q "$PATH_MOUNT"; then
	populate_persistent_storage

	if ! umount "$PATH_MOUNT"; then
		echo "  Couldn't unmount the new fs!"
		exit 9
	fi
fi


exit 0

