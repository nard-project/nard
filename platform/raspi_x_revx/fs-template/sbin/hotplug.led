#!/bin/ash

# Script for freeing up the board ACT LED
# for our custom control.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2017 Ronny Nilsson



# Is the necessary drivers loaded yet?
[ -e /sys/bus/platform/drivers/leds-gpio/unbind ] || exit 0
[ -e /sys/class/gpio/export ] || exit 0

# Has we run before?
[ -e /dev/led-act ] && exit 0

# Check the HW board we are running on
hwCpu=$(grep -iE "^Hardware" /proc/cpuinfo | awk '{ print $3 }')
hwRev=$(grep -iE "^Revision" /proc/cpuinfo | awk '{ print $3 }')
act=""
actInverse=0																	# Is pin active low?


# Is there a kernel LED driver which use Device Tree?
if [ -d /sys/firmware/devicetree/base/soc/leds ]; then

	# Scan for LEDs kernel know of and create
	# descriptive symlinks in /dev.
	for f in /sys/firmware/devicetree/base/soc/leds/*; do
		led=${f##*/}															# Extract name of LED
		[ -d /sys/firmware/devicetree/base/soc/leds/${led} ] || continue;
		[ "$led" = "pwr" ] && continue											# Don't mess with the brown out sensor

		read -t 5 -s label <"/sys/firmware/devicetree/base/soc/leds/${led}/label"	# Name to number mapping

		if [ -w /sys/class/leds/${label}/trigger ]; then
			# We want userspace to control the LED entirely,
			# no fancy kernel features! Accomplish this by
			# using only a fraction of the "oneshot" trigger.
			# We can NOT use the "none" trigger and control
			# brightness manually due to it fires a hotplug
			# event every time we set the LED to off state.
			echo oneshot >/sys/class/leds/${label}/trigger
			echo 950 >/sys/class/leds/${label}/delay_off
			echo 950 >/sys/class/leds/${label}/delay_on
			echo 0 >/sys/class/leds/${label}/invert
			ln -s /sys/class/leds/${label}/invert /dev/led-${led}
		fi
	done

	# This was easy, everything finished.
	exit 0

# Does the system use Device Tree during boot? Then read dynamically from
# it which GPIOs are used for the LEDs. For backwards compatibillity.
elif [ -e /sys/firmware/devicetree/base/aliases/leds ]; then
	read -t -5 -s where <"/sys/firmware/devicetree/base/aliases/leds"

	if [ -e /sys/firmware/devicetree/base${where}/act ]; then
		act=$(od -A n -j 7 -N 1 -td1 \
			/sys/firmware/devicetree/base${where}/act/gpios | \
			tr -d "[[:space:]]")
		actInverse=$(od -A n -j 11 -N 1 -td1 \
			/sys/firmware/devicetree/base${where}/act/gpios | \
			tr -d "[[:space:]]")
	fi

	# Disable the kernel driver which may
	# have been using the ACT led until now. 
	echo "soc:leds" >/sys/bus/platform/drivers/leds-gpio/unbind 2>/dev/null

# Old style manual pin definitions, for backwards compatibillity.
elif [ "$hwCpu" = "BCM2708" ]; then
	# Mapping of Raspberry Pi board revision
	# history to led GPIO as from
	# http://elinux.org/RPi_HardwareHistory
	case $hwRev in
		*0004)																	# B
			act=16
			actInverse=1
			;;
		*0005)																	# B
			act=16
			actInverse=1
			;;
		*0006)																	# B
			act=16
			actInverse=1
			;;
		*0007)																	# A
			act=16
			actInverse=1
			;;
		*0008)																	# A
			act=16
			actInverse=1
			;;
		*0009)																	# A
			act=16
			actInverse=1
			;;
		*000a)																	# ?
			act=16
			actInverse=1
			;;
		*000b)																	# ?
			act=16
			actInverse=1
			;;
		*000c)																	# ?
			act=16
			actInverse=1
			;;
		*000d)																	# B
			act=16
			actInverse=1
			;;
		*000e)																	# B
			act=16
			actInverse=1
			;;
		*000f)																	# B
			act=16
			actInverse=1
			;;
		*0010)																	# B+
			act=47
			;;
		*0011)																	# CM
			act=47
			;;
		*00??)																	# default
			act=47
			;;
	esac

	# Disable the kernel driver which may
	# have been using the ACT led until now. 
	echo "leds-gpio" >/sys/bus/platform/drivers/leds-gpio/unbind 2>/dev/null
fi
	


# Create symlink for ACT led
if [ -n "$act" ]; then
	[ -e /sys/class/gpio/gpio${act} ] || echo $act >/sys/class/gpio/export
	
	if [ -e /sys/class/gpio/gpio${act}/direction ]; then
		echo out >/sys/class/gpio/gpio${act}/direction
		echo $actInverse >/sys/class/gpio/gpio${act}/active_low
		echo 0 >/sys/class/gpio/gpio${act}/value
		ln -s /sys/class/gpio/gpio${act}/value /dev/led-act
	fi
fi


exit 0
