#!/bin/ash

# Startup of the inet daemon
#
# Arguments:
# IFACE = physical name of the interface being processed
# MODE = start or stop


# Path to executable binary
daemon="/usr/sbin/inetd"
name="inetd"
pathLockFile="/var/lock/inetd"


[ -x "$daemon" ] || exit 0


# Pull library functions and settings.
source "/lib/nard/net.sh" || exit


# Is this a loopback or point to point
# interface? Then ignore it.
[ -z "$IFACE" ] || is_nic_regular "$IFACE" || exit 0


(
	# Is there another process already running? then wait.
	/usr/bin/flock -x 20 || exit 1

	start-stop-daemon -n "$name" -N 1 -x "$daemon"								\
		-p "/var/run/${name}.pid" -q -K
	: >"/var/run/${name}.pid"

	echo "Starting ${name}..."

	(exec 20>&-; \
		start-stop-daemon -n "$name" -x "$daemon" -q -S)						# Close the lockfile for childrens

	# Short wait for daemon to either start up or die.
	# Otherwise we might launch multiple instances if
	# this script is run twice in parallell.
	for try in 1 2 3 4 5 6 7 8 9 10 11 12 14 14 15; do
		sleep 0.2
		[ -s "/var/run/${name}.pid" ] && break
		pidof "$name" >/dev/null || break
	done
) 20>"$pathLockFile"


exit 0
