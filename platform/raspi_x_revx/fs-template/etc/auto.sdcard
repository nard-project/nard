#!/bin/ash

# Scan first SD card for valid partitions. This
# is dynamic since it's not for sure we have a
# SD card. If none are found we either emulate
# one or let a custom user script do a network
# mount.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2019 Ronny Nilsson



optsCom="nosuid,nodev,noatime,nodiratime,norelatime"
optsFat="noexec,dirsync,flush,quiet,showexec,discard,codepage=850,iocharset=iso8859-1"
optsF2fs="nouser_xattr,noacl,fastboot,discard"
optsExt2="check=none"


# Export to environment variables what hw we're running on.
if [ -e /etc/board ]; then
	read -t 5 -s BOARDS </etc/board
	read -t 5 -s PRODUCTS </etc/product
else
	BOARDS="UNKNOWN"
	PRODUCTS="UNKNOWN"
fi
BOARD=${BOARDS##* }
PRODUCT=${PRODUCTS##* }
export BOARD BOARDS
export PRODUCT PRODUCTS


# Is there a volatile emulated SD card for loop mount?
# Then prepare it.
pathLoFl="/var/spool/volatile-storage-user.ext2"
[ -s "$pathLoFl" ] && {
	pathLo=$(losetup -a | grep -F "$pathLoFl" | cut -d : -f 1)
	[ -z "$pathLo" ] && {
		losetup -f "$pathLoFl"
		pathLo=$(losetup -a | grep -F "$pathLoFl" | cut -d : -f 1)
	}
}


# For systems without SD card we support custom scripts
# for accessing a remote Network Block Device
# http://nbd.sf.net/ However possibly Windows shares can
# be used too (untested):
#   1) rm /boot
#   2) mkdir -p /boot
#   3) mount -t cifs ....... /boot
for X in $BOARDS; do
	[ -e "${0}.${X}" ] || continue
	source "${0}.${X}"
done
for X in $PRODUCTS; do
	[ -e "${0}.${X}" ] || continue
	source "${0}.${X}"
done


# Scan all SD card partitions for the filesystems
# we know should be there.
for dev in /dev/mmcblk0p* $pathLo; do

	# Is there such a block device?
	[ -b $dev ] || continue

	# Find out what filesystem the partition has.
	TYPE=""
	opts=""
	eval $(blkid $dev | cut -d : -f 2)
	[ -z "$TYPE" ] && continue

	# What automount key (partition) did user want to mount?
	case $1 in
		boot)
			[ "$TYPE" = "vfat" ] && opts="$optsFat"
			;;
		user)
			[ "$TYPE" = "f2fs" ] && opts="$optsF2fs"
			[ "$TYPE" = "ext2" ] && opts="$optsExt2"
			;;
	esac

	[ -n "$opts" ] && {
		echo "-fstype=$TYPE,${opts},${optsCom}  :${dev}"
		exit 0
	}
done


exit 0
