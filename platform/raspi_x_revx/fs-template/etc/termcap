# simplified termcap file for eLinux
# only has enough to support a TERM=linux setting (with color)

# The IBM PC alternate character set.  Plug this into any Intel console entry.
# We use \E[11m for rmacs rather than \E[12m so the <acsc> string can use the
# ROM graphics for control characters such as the diamond, up- and down-arrow.
# This works with the System V, Linux, and BSDI consoles.  It's a safe bet this
# will work with any Intel console, they all seem to have inherited \E[11m
# from the ANSI.SYS de-facto standard.
klone+acs|alternate character set for ansi.sys displays:\
        :ac=`\004a\261f\370g\361h\260j\331k\277l\332m\300n\305o~q\304r\362s_t\30
3u\264v\301w\302x\263y\371z\372{\373|\374}\375~\376.\031-\030\054\021+^P0\333p\3
04r\304y\363z\362{\343|\330}\234:\
        :ae=\E[10m:as=\E[11m:

# Highlight controls corresponding to the ANSI.SYS standard.  Most 
# console drivers for Intel boxes obey these.  Makes the same assumption
# about \E[11m as klone+acs.  True ANSI would have rmso=\E[27m, rmul=\E[24m,
# but this isn't a documented feature of ANSI.SYS.
klone+sgr|attribute control for ansi.sys displays:\
        :S2=\E[11m:S3=\E[10m:ae=\E[11m:as=\E11m:mb=\E[5m:\
        :md=\E[1m:me=\E[0;10m:mk=\E[8m:mr=\E[7m:\
        :..sa=\E[0;10%?%p1%t;7%;%?%p2%t;4%;%?%p3%t;7%;%?%p4%t;5%;%?%p6%t;1%;%?%p7%t;8%;%?%p9%t;11%;m:\
        :se=\E[m:so=\E[7m:ue=\E[m:us=\E[4m:

klone+color|color control for ansi.sys and ISO6429-compatible displays:\
        :Co#8:NC#3:pa#64:\
        :AB=\E[4%p1%dm:AF=\E[3%p1%dm:Sb=\E[%+(m:Sf=\E[%+^^m:\
        :op=\E[37;40m:

# From: Eric S. Raymond <esr@snark.thyrsus.com> 9 Nov 1995
linux|linux console:\
        :am:eo:mi:ms:ut:xn:xo:\
        :co#80:it#8:li#25:\
        :&7=^Z:@7=\E[4~:AL=\E[%dL:DC=\E[%dP:DL=\E[%dM:\
        :F1=\E[23~:F2=\E[24~:F3=\E[25~:F4=\E[26~:F5=\E[28~:\
        :F6=\E[29~:F7=\E[31~:F8=\E[32~:F9=\E[33~:FA=\E[34~:\
        :IC=\E[%d@:K2=\E[G:al=\E[L:bl=^G:cd=\E[J:ce=\E[K:\
        :ch=\E[%i%dG:cl=\E[H\E[J:cm=\E[%i%d;%dH:cr=^M:\
        :cs=\E[%i%d;%dr:ct=\E[3g:cv=\E[%i%dd:dc=\E[P:dl=\E[M:\
        :do=^J:ei=\E[4l:ho=\E[H:ic=\E[@:im=\E[4h:k1=\E[[A:\
        :k2=\E[[B:k3=\E[[C:k4=\E[[D:k5=\E[[E:k6=\E[17~:\
        :k7=\E[18~:k8=\E[19~:k9=\E[20~:k;=\E[21~:kB=\E[Z:\
        :kD=\E[3~:kI=\E[2~:kN=\E[6~:kP=\E[5~:kb=^H:kd=\E[B:\
        :kh=\E[1~:kl=\E[D:kr=\E[C:ku=\E[A:le=^H:mr=\E[7m:\
        :nd=\E[C:nw=^M^J:r1=\Ec:rc=\E8:sc=\E7:sf=^J:sr=\EM:\
        :st=\EH:ta=^I:u6=\E[%i%d;%dR:u7=\E[6n:u8=\E[?6c:\
        :u9=\E[c:up=\E[A:vb=200\E[?5h\E[?5l:ve=\E[?25h:\
        :vi=\E[?25l:tc=klone+sgr:tc=klone+color:tc=klone+acs:
linux-m|Linux console no color:\
        :Co@:pa@:\
        :AB@:AF@:Sb@:Sf@:tc=linux:

xf|xterm-xfree86|XFree86 xterm:\
	:is=\E[!p\E[?3;4l\E[4l\E>:\
	:rs=\E[!p\E[?3;4l\E[4l\E>:\
	:AL=\E[%dL:DL=\E[%dM:DC=\E[%dP:DO=\E[%dB:UP=\E[%dA:\
	:LE=\E[%dD:RI=\E[%dC:\
	:al=\E[L:am:bl=^G:\
	:cd=\E[J:ce=\E[K:cl=\E[H\E[2J:cm=\E[%i%d;%dH:co#80:\
	:cs=\E[%i%d;%dr:ct=\E[3g:\
	:dc=\E[P:dl=\E[M:ho=\E[H:\
	:im=\E[4h:ei=\E[4l:mi:\
	:ks=\E[?1h\E=:ke=\E[?1l\E>:\
	:k1=\EOP:k2=\EOQ:k3=\EOR:k4=\EOS:\
	:k5=\E[15~:k6=\E[17~:k7=\E[18~:k8=\E[19~:k9=\E[20~:\
	:k;=\E[21~:F1=\E[23~:F2=\E[24~:\
	:kn#12:\
	:kH=\E[4~::@7=\E[4~:kh=\E[1~:\
	:@0=\E[1~:kI=\E[2~:kD=\177:\
	:*6=\E[4~:kP=\E[5~:kN=\E[6~:\
	:km:\
	:kb=^H:ku=\EOA:kd=\EOB:kr=\EOC:kl=\EOD:\
	:li#24:md=\E[1m:me=\E[m^O:mr=\E[7m:ms:nd=\E[C:\
	:eA=\E)0:as=^N:ae=^O:ml=\El:mu=\Em:\
	:sc=\E7:rc=\E8:sf=\n:so=\E[7m:se=\E[27m:sr=\EM:st=\EH:\
	:ti=\E7\E[?47h:te=\E[2J\E[?47l\E8:\
	:vi=\E[?25l:ve=\E[?25h:\
	:up=\E[A:us=\E[4m:ue=\E[24m:xn:\
	:ut:Co#8:op=\E[39;49m:AB=\E[4%dm:AF=\E[3%dm:\
	:pa#64:Sf=\E[3%dm:Sb=\E[4%dm:

xterm-debian|xterm with modifications to follow Debian keyboard policy:\
	:tc=xterm-redhat:
xterm-redhat|xterm with modifications to follow Debian keyboard policy:\
	:kb=\177:kD=\E[3~:F3=\E[25~:F4=\E[26~:F5=\E[28~:\
	:F6=\E[29~:F7=\E[31~:F8=\E[32~:F9=\E[33~:FA=\E[34~:\
	:tc=xterm-xfree86:

# This is the only entry which you should have to customize, since "xterm"
# is widely used for a variety of incompatible terminal emulations including
# color_xterm and rxvt.
v0|xterm|X11 terminal emulator:\
	:tc=xterm-redhat:
#       :tc=xterm-xfree86:
#       :tc=xterm-r6:

# ansi -- this terminfo expresses the largest subset of X3.64 that will fit in
# standard terminfo.  Assumes ANSI.SYS-compatible attributes and color
# From: Eric S. Raymond <esr@snark.thyrsus.com> Nov 6 1995
ansi|ansi/pc-term compatible with color:\
        :u6=\E[%i%d;%dR:u7=\E[6n:..u8=\E[?%[;0123456789]c:\
        :u9=\E[c:tc=klone+color:tc=klone+acs:tc=klone+sgr:tc=ansi-m:


