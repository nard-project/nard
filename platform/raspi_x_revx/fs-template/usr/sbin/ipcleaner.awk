# Simple IP and MASK verifyer and cleaner.
# Given an IPv4 address 10.060.5.77, remove
# the leading zero.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014 Ronny Nilsson

BEGIN {
	a=-1;
	b=-1;
	c=-1;
	d=-1;
}


/^[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$/{
	a=$1+0;
	b=$2+0;
	c=$3+0;
	d=$4+0;
}


END { 
	e=0;
	if(a<0) e=1;
	if(a>255) e=1;
	if(b<0) e=1;
	if(b>255) e=1;
	if(c<0) e=1;
	if(c>255) e=1;
	if(d<0) e=1;
	if(d>255) e=1;

	if(e==1) {
		print "Error, illegal address "$0;
		exit 1;
	}
	else {
		print a"."b"."c"."d;
		exit 0;
	}
}

