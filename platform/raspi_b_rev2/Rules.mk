
#-------------------------------------------
# Common hardware recipe for products
# running on Raspberry Pi model B rev 2.
#-------------------------------------------



#-----------------------------
# Get defines which tell what hw and
# sw we use and thus need (if any).
BOARD_DEPS += raspi_b_revx
-include $(PATH_TOP)/platform/raspi_b_revx/Rules.mk

