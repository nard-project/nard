
#-------------------------------------------
# Product "mediaplayer" recipe
#-------------------------------------------


# List of applications this product needs. Packages
# will be built in the listed order (unless you
# explicitly define the dependencies).
PKGS_APPS += salsa-lib/salsa-lib-0.1.3
PKGS_APPS += mpg123/mpg123-1.21.0
PKGS_APPS += alsa-utils/alsa-utils-1.0.25
PKGS_APPS += jpeg-lib/jpeg-lib-8d
PKGS_APPS += fbi/fbi-1.31
PKGS_APPS += $(if $(HAS_NARD_ADDONS), videocore-libs/videocore-libs-0.02)		# Include VC libs if they are available.
PKGS_APPS += ffmpeg/https//github.com/FFmpeg/FFmpeg.git^n5.0.1
PKGS_APPS += openh264/https//github.com/cisco/openh264.git^openh264v2.3.0


# List of application patches this platform needs.
fbi-1.31.patches += fbtools.c
fbi-1.31.patches += fbi.c
fbi-1.31.patches += fb-gui.c
fbi-1.31.patches += fb-gui.h
fbi-1.31.patches += mk/Autoconf.mk

videocore-libs-0.01.patches += Makefile
videocore-libs-0.01.patches += Rules.mk
videocore-libs-0.01.patches += host_applications/framework/common/Makefile
videocore-libs-0.01.patches += host_applications/linux/libs/bcm_host/Makefile
videocore-libs-0.01.patches += interface/vcos/Makefile
videocore-libs-0.01.patches += interface/khronos/Makefile
videocore-libs-0.01.patches += interface/vchiq_arm/Makefile
videocore-libs-0.01.patches += interface/vmcs_host/Makefile
videocore-libs-0.01.patches += interface/mmal/core/Makefile
videocore-libs-0.01.patches += interface/mmal/vc/Makefile
videocore-libs-0.01.patches += interface/mmal/util/Makefile
videocore-libs-0.01.patches += host_applications/linux/apps/gencmd/Makefile
videocore-libs-0.01.patches += host_applications/linux/apps/hello_pi/libs/ilclient/Makefile
videocore-libs-0.01.patches += host_applications/linux/apps/hello_pi/hello_video/Makefile
videocore-libs-0.01.patches += host_applications/linux/apps/hello_pi/hello_tiger/Makefile
videocore-libs-0.01.patches += host_applications/linux/apps/hello_pi/hello_teapot/Makefile
videocore-libs-0.01.patches += host_applications/linux/apps/tvservice/Makefile
videocore-libs-0.02.patches += Makefile
videocore-libs-0.02.patches += Rules.mk
videocore-libs-0.02.patches += interface/vcos/Makefile
videocore-libs-0.02.patches += interface/vcos/vcos_types.h
videocore-libs-0.02.patches += interface/khronos/Makefile
videocore-libs-0.02.patches += interface/vchiq_arm/Makefile
videocore-libs-0.02.patches += interface/vmcs_host/Makefile
videocore-libs-0.02.patches += interface/vmcs_host/linux/vcfiled/Makefile
videocore-libs-0.02.patches += interface/mmal/core/Makefile
videocore-libs-0.02.patches += interface/mmal/vc/Makefile
videocore-libs-0.02.patches += interface/mmal/util/Makefile
videocore-libs-0.02.patches += interface/mmal/openmaxil/Makefile
videocore-libs-0.02.patches += interface/mmal/components/Makefile
videocore-libs-0.02.patches += host_applications/linux/apps/gencmd/Makefile
videocore-libs-0.02.patches += host_applications/linux/apps/hello_pi/libs/ilclient/Makefile
videocore-libs-0.02.patches += host_applications/linux/apps/hello_pi/hello_video/Makefile
videocore-libs-0.02.patches += host_applications/linux/apps/hello_pi/hello_tiger/Makefile
videocore-libs-0.02.patches += host_applications/linux/apps/hello_pi/hello_teapot/Makefile
videocore-libs-0.02.patches += host_applications/linux/apps/tvservice/Makefile
videocore-libs-0.02.patches += host_applications/linux/apps/raspicam/Makefile
videocore-libs-0.02.patches += host_applications/framework/common/Makefile
videocore-libs-0.02.patches += host_applications/linux/libs/bcm_host/Makefile
videocore-libs-0.02.patches += host_applications/vmcs/test_apps/mmalplay/Makefile
videocore-libs-0.02.patches += host_applications/linux/libs/sm/Makefile
videocore-libs-0.02.patches += host_applications/linux/apps/smem/Makefile
videocore-libs-0.02.patches += containers/Makefile
videocore-libs-0.02.patches += containers/rtp/Makefile
videocore-libs-0.02.patches += containers/Rules.mk

openh264-openh264v2.3.0.patches += build/platform-nard.mk


# Explicit package dependencies (optional, but
# nice to have for librarys)
mpg123/mpg123-1.21.0: salsa-lib/salsa-lib-0.1.3
alsa-utils/alsa-utils-1.0.25: salsa-lib/salsa-lib-0.1.3
fbi/fbi-1.31: jpeg-lib/jpeg-lib-8d
videocore-libs/videocore-libs-0.02: linux-kernel/linux-rpi-4.4.y
ffmpeg/https//github.com/FFmpeg/FFmpeg.git^n5.0.1: openssl-lib/openssl-lib-1.1.1a
ffmpeg/https//github.com/FFmpeg/FFmpeg.git^n5.0.1: salsa-lib/salsa-lib-0.1.3
ffmpeg/https//github.com/FFmpeg/FFmpeg.git^n5.0.1: z-lib/z-lib-1.2.8
ffmpeg/https//github.com/FFmpeg/FFmpeg.git^n5.0.1: openh264/https//github.com/cisco/openh264.git^openh264v2.3.0
ifdef HAS_NARD_ADDONS
	ffmpeg/https//github.com/FFmpeg/FFmpeg.git^n5.0.1: videocore-libs/videocore-libs-0.02
endif


#-----------------------------
# Get defines which tell what hw and
# sw we use and thus need (if any).
PRODUCT_DEPS += skeleton
include $(PATH_TOP)/platform/skeleton/Rules.mk

