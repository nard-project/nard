
#-------------------------------------------
# Common hardware recipe for products
# running on Raspberry Pi model 2B
#-------------------------------------------

# Default compiler options for applications. Here we depend
# on which compiler (toolchain) is in use. The oldest one
# doesn't know of ARM Cortex-A7, but later does.
ifeq ($(CROSS_TOOLCHAIN_VER),1.18.0)
    CROSS_TUNE ?= -march=armv7-a -mtune=cortex-a5
else
    CROSS_TUNE ?= -march=armv7-a -mtune=cortex-a7
endif

CROSS_ARCH ?= -D__LINUX_ARM_ARCH__=7
CROSS_FPU ?= -mfpu=neon-vfpv4 -mfloat-abi=hard -mhard-float
CROSS_HAS_NEON ?= 1



#-----------------------------
# Get defines which tell what hw and
# sw we use and thus need (if any).
BOARD_DEPS += raspi_x_revx
-include $(PATH_TOP)/platform/raspi_x_revx/Rules.mk

