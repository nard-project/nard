
#-------------------------------------------
# Common hardware recipe for products
# running on Raspberry Pi model 1B
#-------------------------------------------

# Default compiler options for applications
CROSS_TUNE ?= -march=armv6zk -mtune=arm1176jzf-s
CROSS_ARCH ?= -D__LINUX_ARM_ARCH__=6
CROSS_FPU ?= -mfpu=vfp -mfloat-abi=hard -mhard-float



#-----------------------------
# Get defines which tell what hw and
# sw we use and thus need (if any).
BOARD_DEPS += raspi_x_revx
-include $(PATH_TOP)/platform/raspi_x_revx/Rules.mk

