
#-------------------------------------------
# Product recipe
#-------------------------------------------


# List of applications this product needs. Packages
# will be built in the listed order (unless you
# explicitly define the dependencies).
PKGS_APPS += ser2net/ser2net-3.5.2


# List of application patches this platform needs.
linux-rpi-3.10.y.patches += arch/arm/mach-bcm2708/include/mach/uncompress.h
linux-rpi-3.10.y.patches += arch/arm/mach-bcm2708/include/mach/debug-macro.S
ser2net-3.5.2.patches += dataxfer.c


#-----------------------------
# Get defines which tell what hw and
# sw we use and thus need (if any).
PRODUCT_DEPS += skeleton
include $(PATH_TOP)/platform/skeleton/Rules.mk

