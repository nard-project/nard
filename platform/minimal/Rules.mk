
#-------------------------------------------
# Product "minimal" recipe
#-------------------------------------------


# The HW we will run on
export BOARD ?= raspi_b_revx


# List of applications this product needs. Packages
# will be built in the listed order (unless you
# explicitly define the dependencies).
PKGS_APPS += linux-kernel/linux-rpi-4.4.y


#-----------------------------
# Get defines which tell what hw and
# sw we use and thus need (if any).
PRODUCT_DEPS +=
-include $(PATH_TOP)/platform/<parent-product>/Rules.mk

