/*
 * Dummy functions for NOT using the RPi UART.
 *
 */

static inline void putc(int c) {}
static inline void flush(void) {}
static inline void arch_decomp_setup(void) {}
