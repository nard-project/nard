commit 5762758699e1ddab22bf4c14eb225941761c52c8
Author: Dave Stevenson <dave.stevenson@raspberrypi.org>
Date:   Wed Jun 13 15:21:10 2018 +0100

    net: lan78xx: Disable TCP Segmentation Offload (TSO)
    
    TSO seems to be having issues when packets are dropped and the
    remote end uses Selective Acknowledge (SACK) to denote that
    data is missing. The missing data is never resent, so the
    connection eventually stalls.
    
    There is a module parameter of enable_tso added to allow
    further debugging without forcing a rebuild of the kernel.
    
    https://github.com/raspberrypi/linux/issues/2449
    https://github.com/raspberrypi/linux/issues/2482
    
    Signed-off-by: Dave Stevenson <dave.stevenson@raspberrypi.org>

diff --git a/drivers/net/usb/lan78xx.c b/drivers/net/usb/lan78xx.c
index 28c47345420a..9eada927f572 100644
--- a/drivers/net/usb/lan78xx.c
+++ b/drivers/net/usb/lan78xx.c
@@ -289,6 +415,15 @@ static int msg_level = -1;
 module_param(msg_level, int, 0);
 MODULE_PARM_DESC(msg_level, "Override default message level");
 
+/* TSO seems to be having some issue with Selective Acknowledge (SACK) that
+ * results in lost data never being retransmitted.
+ * Disable it by default now, but adds a module parameter to enable it for
+ * debug purposes (the full cause is not currently understood).
+ */
+static bool enable_tso;
+module_param(enable_tso, bool, 0644);
+MODULE_PARM_DESC(enable_tso, "Enables TCP segmentation offload");
+
 static int lan78xx_read_reg(struct lan78xx_net *dev, u32 index, u32 *data)
 {
 	u32 *buf = kmalloc(sizeof(u32), GFP_KERNEL);
@@ -2334,8 +2921,14 @@ static int lan78xx_bind(struct lan78xx_net *dev, struct usb_interface *intf)
 	if (DEFAULT_RX_CSUM_ENABLE)
 		dev->net->features |= NETIF_F_RXCSUM;
 
-	if (DEFAULT_TSO_CSUM_ENABLE)
-		dev->net->features |= NETIF_F_TSO | NETIF_F_TSO6 | NETIF_F_SG;
+	if (DEFAULT_TSO_CSUM_ENABLE) {
+		dev->net->features |= NETIF_F_SG;
+		/* Use module parameter to control TCP segmentation offload as
+		 * it appears to cause issues.
+		 */
+		if (enable_tso)
+			dev->net->features |= NETIF_F_TSO | NETIF_F_TSO6;
+	}
 
 	dev->net->hw_features = dev->net->features;
 
