commit 18956a569392cbbde22b5f67ae5dd096e551cd1e
Author: Phil Elwell <phil@raspberrypi.org>
Date:   Thu Apr 5 14:46:11 2018 +0100

    lan78xx: Move enabling of EEE into PHY init code
    
    BugLink: http://bugs.launchpad.net/bugs/1784025
    
    Enable EEE mode as soon as possible after connecting to the PHY, and
    before phy_start. This avoids a second link negotiation, which speeds
    up booting and stops the interface failing to become ready.
    
    See: https://github.com/raspberrypi/linux/issues/2437
    
    Signed-off-by: Phil Elwell <phil@raspberrypi.org>
    (cherry picked from commit ffd7bf4085b09447e5db96edd74e524f118ca3fe
    https://github.com/raspberrypi/linux rpi-4.9.y)
    Signed-off-by: Paolo Pisati <paolo.pisati@canonical.com>

diff --git a/drivers/net/usb/lan78xx.c b/drivers/net/usb/lan78xx.c
index 2bf2495577bc..e7d4ef5b6fd3 100644
--- a/drivers/net/usb/lan78xx.c
+++ b/drivers/net/usb/lan78xx.c
@@ -1549,6 +1549,22 @@ static int lan78xx_phy_init(struct lan78xx_net *dev)
 			      SUPPORTED_1000baseT_Full |
 			      SUPPORTED_Pause | SUPPORTED_Asym_Pause);
 
+	if (of_property_read_bool(dev->udev->dev.of_node,
+				  "microchip,eee-enabled")) {
+		struct ethtool_eee edata;
+		memset(&edata, 0, sizeof(edata));
+		edata.cmd = ETHTOOL_SEEE;
+		edata.advertised = ADVERTISED_1000baseT_Full |
+				   ADVERTISED_100baseT_Full;
+		edata.eee_enabled = true;
+		edata.tx_lpi_enabled = true;
+		if (of_property_read_u32(dev->udev->dev.of_node,
+					 "microchip,tx-lpi-timer",
+					 &edata.tx_lpi_timer))
+			edata.tx_lpi_timer = 600; /* non-aggressive */
+		(void)lan78xx_set_eee(dev->net, &edata);
+	}
+
 	/* Set LED modes:
 	 * led: 0=link/activity          1=link1000/activity
 	 *      2=link100/activity       3=link10/activity
@@ -1991,22 +2007,6 @@ static int lan78xx_open(struct net_device *net)
 
 	netif_dbg(dev, ifup, dev->net, "phy initialised successfully");
 
-	if (of_property_read_bool(dev->udev->dev.of_node,
-				  "microchip,eee-enabled")) {
-		struct ethtool_eee edata;
-		memset(&edata, 0, sizeof(edata));
-		edata.cmd = ETHTOOL_SEEE;
-		edata.advertised = ADVERTISED_1000baseT_Full |
-				   ADVERTISED_100baseT_Full;
-		edata.eee_enabled = true;
-		edata.tx_lpi_enabled = true;
-		if (of_property_read_u32(dev->udev->dev.of_node,
-					 "microchip,tx-lpi-timer",
-					 &edata.tx_lpi_timer))
-			edata.tx_lpi_timer = 600; /* non-aggressive */
-		(void)lan78xx_set_eee(net, &edata);
-	}
-
 	/* for Link Check */
 	if (dev->urb_intr) {
 		ret = usb_submit_urb(dev->urb_intr, GFP_KERNEL);
