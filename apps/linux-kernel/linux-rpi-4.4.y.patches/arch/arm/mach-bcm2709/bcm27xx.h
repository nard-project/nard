
#ifndef MACH_BCM27XX_COMMON_INC
#define MACH_BCM27XX_COMMON_INC

#include <linux/reboot.h>

#ifdef CONFIG_SMP
extern struct smp_operations  bcm27xx_smp_ops __initdata;
#endif


void __init bcm_map_io(unsigned long peri_base, unsigned long bus_offs, unsigned long io_mask);
void bcm27xx_restart(enum reboot_mode mode, const char *cmd);
void __init bcm27xx_init(unsigned int boardrev, unsigned int serial, unsigned int reboot_mode);
void __init bcm27xx_timer_init(void);
void __init bcm27xx_init_early(void);
void __init bcm27xx_board_reserve(void);


#endif

