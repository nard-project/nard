/*
 *  arch/arm/mach-bcn2708/include/mach/uncompress.h
 *
 *  Unified BCM27xx support 2016 Ronny Nilsson
 *  Copyright (C) 2010 Broadcom
 *  Copyright (C) 2003 ARM Limited
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/io.h>
#include <linux/amba/serial.h>
#include <mach/platform.h>
#include <linux/byteorder/little_endian.h>

#define UART_BAUD 115200

#define BCM2708_UART_DR   __io(UART0_BASE + UART01x_DR)
#define BCM2708_UART_FR   __io(UART0_BASE + UART01x_FR)
#define BCM2708_UART_IBRD __io(UART0_BASE + UART011_IBRD)
#define BCM2708_UART_FBRD __io(UART0_BASE + UART011_FBRD)
#define BCM2708_UART_LCRH __io(UART0_BASE + UART011_LCRH)
#define BCM2708_UART_CR   __io(UART0_BASE + UART011_CR)
#define BCM2708_GPIO_FSEL __io(GPIO_BASE + 0x4ul)

#define FDT_MAGIC         __be32_to_cpu(0xd00dfeedu)

/* ARM "machine", educed from device tree */
struct machs_t {
	const char *compatible;
	const char *model;
// TODO: change to u32 type
	unsigned long periBase;
	unsigned long busOffset;
	unsigned long ioMask;
	unsigned long uartClk;
};

struct machs_t machs[4];
struct machs_t *mach;
unsigned long bcm27xx_peri_base;
unsigned long bcm27xx_bus_offs;
unsigned long bcm27xx_io_mask;

/*
 * This does not append a newline
 */
static inline void putc(int c)
{
	u32 gpfsel1Orig, gpfsel1uart0;

	if(!IS_ENABLED(CONFIG_SERIAL_CORE_CONSOLE)) return;
	if(!mach) return;

	/* Activate the full PL011 UART0 Tx pin in
	 * the RPi pin header, but remember what were
	 * there before our change as well. */
	gpfsel1Orig = __raw_readl(BCM2708_GPIO_FSEL);
	gpfsel1uart0 = gpfsel1Orig & ~(7ul << 12);
	gpfsel1uart0 |= (4ul << 12);
	__raw_writel(gpfsel1uart0, BCM2708_GPIO_FSEL);

	// Send character
	__raw_writel(c, BCM2708_UART_DR);

	// Wait for full completion of transmission
	while (__raw_readl(BCM2708_UART_FR) & UART01x_FR_BUSY) barrier();

	/* Possibly restore the Mini UART as pin header
	 * console UART (due to we don't know whether
	 * there will be any more data to send). */
	__raw_writel(gpfsel1Orig, BCM2708_GPIO_FSEL);
}

static inline void flush(void)
{
	int fr;

	if(!IS_ENABLED(CONFIG_SERIAL_CORE_CONSOLE)) return;

	do {
		fr = __raw_readl(BCM2708_UART_FR);
		barrier();
	} while ((fr & (UART011_FR_TXFE | UART01x_FR_BUSY)) != UART011_FR_TXFE);
}


/* Search the stack backwards for the atags pointer. We know it has
 * been pushed there in a certain layout (in head.S). */
static noinline int find_fdt(u32 **atags_pointer)
{
	const unsigned long *pFrame = __builtin_frame_address(0);
	extern unsigned char *output_data;
	const unsigned char *kexecAdr;
	unsigned int arch_type, i;

	kexecAdr = NULL;
	arch_type = 0;
	*atags_pointer = 0;

	/* Some sanity checks */
	if(!(unsigned long) output_data) return 0;
	if((unsigned long) pFrame < 1024u) return 0;

	/* Scan approximately three worst case frames */
	for(i = 0; i < 96 && (kexecAdr != output_data ||
			arch_type != __machine_arch_type || 
			**atags_pointer != FDT_MAGIC); i++) {
		kexecAdr = (const unsigned char*) pFrame[i];
		arch_type = pFrame[i + 3];
		*atags_pointer = (u32*) pFrame[i + 4];
	}

	/* Did we found it? Then return true. */
	return (kexecAdr == output_data && *atags_pointer &&
		arch_type == __machine_arch_type &&
		**atags_pointer == FDT_MAGIC);
}


/* Scan flattened device tree for magic strings we know
 * should be there and map it to an old style ARM machine type.
 * Thus we do it quick and dirty; don't parse the tree as it
 * would add lot's of complexity to the as-simple-as-possible
 * uncompressor. */
static int find_mach(const u32* const atags_pointer)
{
	int fdtLen, i, j, len;

	/* Length of the device tree is one int below the magic */
	fdtLen = __be32_to_cpu(atags_pointer[1]);
	if(fdtLen <= 0 || fdtLen > 1048576) return -1;

	/* Search the device tree for magic strings we know are
	 * there. Seach for later models first, then older ones
	 * (due to how the device tree is formated). */
	for(j = ARRAY_SIZE(machs) - 1; j >= 0 && !mach; j--) {
		len = strlen(machs[j].compatible) + 1;

		for(i = 0; i < fdtLen - len && !mach; i++) {
			if(memcmp((char*) atags_pointer + i, 
					machs[j].compatible, len) == 0) {
				mach = machs + j;
				return 1;
			}
		}
	}

	return 0;
}


/* We have found which ARM machine we're running on. Now go set it up. */
static int setup_mach(void)
{
	if(!mach) return 0;
	
	bcm27xx_peri_base = mach->periBase;
	bcm27xx_bus_offs = mach->busOffset;
	bcm27xx_io_mask = mach->ioMask;
	
	return 1;
}


static noinline void arch_decomp_setup(void)
{
	u32 *atags_pointer;
	int temp, div, rem, frac;

	/* Magic machine strings we know are in the device tree.
	 * This table must be populated in runtime due to there
	 * are no .data section. */
	machs[0].compatible = "brcm,bcm2835";
	machs[0].model = "BCM2835";
	machs[0].periBase = 0x20000000ul;
	machs[0].busOffset = 0x40000000ul;
	machs[0].ioMask = 0x0ffffffful;
	machs[0].uartClk = 48000000ul;
	machs[1].compatible = "brcm,bcm2708";
	machs[1].model = "BCM2708";
	machs[1].periBase = 0x20000000ul;
	machs[1].busOffset = 0x40000000ul;
	machs[1].ioMask = 0x0ffffffful;
	machs[1].uartClk = 48000000ul;
	machs[2].compatible = "brcm,bcm2709";
	machs[2].model = "BCM2709";
	machs[2].periBase = 0x3f000000ul;
	machs[2].busOffset = 0xc0000000ul;
	machs[2].ioMask = 0x00fffffful;
	machs[2].uartClk = 48000000ul;
	machs[3].compatible = "brcm,bcm2710";
	machs[3].model = "BCM2710";
	machs[3].periBase = 0x3f000000ul;
	machs[3].busOffset = 0xc0000000ul;
	machs[3].ioMask = 0x00fffffful;
	machs[3].uartClk = 48000000ul;

	/* What ARM machine are we running? Try to scan the
	 * device tree if we got one by the bootloader. This
	 * is not a fully fledged fdt parser, we do lots of
	 * simplifications. */
	mach = 0;
	if(find_fdt(&atags_pointer)) find_mach(atags_pointer);
	if(!setup_mach()) return;

	temp = 16 * UART_BAUD;
	div = mach->uartClk / temp;
	rem = mach->uartClk % temp;
	temp = (8 * rem) / UART_BAUD;
	frac = (temp >> 1) + (temp & 1);

	/* Make sure the UART is disabled before we start */
	__raw_writel(0, BCM2708_UART_CR);

	// Does user realy want a serial console?
	if(!IS_ENABLED(CONFIG_SERIAL_CORE_CONSOLE)) return;

	/* Set the baud rate */
	__raw_writel(div, BCM2708_UART_IBRD);
	__raw_writel(frac, BCM2708_UART_FBRD);

	/* Set the UART to 8n1, FIFO enabled */
	__raw_writel(UART01x_LCRH_WLEN_8 | UART01x_LCRH_FEN, BCM2708_UART_LCRH);

	/* Enable the UART */
	__raw_writel(UART01x_CR_UARTEN | UART011_CR_TXE, BCM2708_UART_CR);

	putstr("\nARM machine: ");
	putstr(mach->model);
	putstr("\n");
}

/*
 * nothing to do
 */
#define arch_decomp_wdog()
