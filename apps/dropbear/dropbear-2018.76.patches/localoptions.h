
/* No need for small code. We have enough memory. */
#define DROPBEAR_SMALL_CODE 0

#define DROPBEAR_BLOWFISH 1

/* Use only one host key. Not three as per default. */
#define DROPBEAR_RSA 1
#define DROPBEAR_DSS 0
#define DROPBEAR_ECDSA 0

/* Maximum number of failed authentication tries. */
#define MAX_AUTH_TRIES 3

/* There is no SFTP server in Nard (yet). */
#define DROPBEAR_SFTPSERVER 0

/* Connection keepalives. */
#define DEFAULT_KEEPALIVE 20
#define DEFAULT_KEEPALIVE_LIMIT 4
#define DEFAULT_IDLE_TIMEOUT 345600


#define DEFAULT_PATH "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin"

