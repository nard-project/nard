
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2016 Ronny Nilsson


include ../../Rules.mk															# Common used stuff

include $(PATH_APPS)/z-lib/Rules.mk


#-----------------------------													# Standard targets
.PHONY: all $(PKG_VER)
all: $(PATH_FS)/.nard-$(PKG_NAME)
$(PKG_VER): $(PATH_FS)/.nard-$(PKG_NAME)


.PHONY: install
install: $(PATH_FS)/.nard-$(PKG_NAME)
$(PATH_FS)/.nard-$(PKG_NAME): $(PKG_VER)/.nard-build
	$(MAKE) -C "$(PKG_VER)" DESTDIR="$(CURDIR)/$(PKG_VER)/staging" install
	find "$(PKG_VER)/staging" -name "*.la" | \
		xargs sed -ire "s|/usr/lib|$(CURDIR)/$(PKG_VER)/staging/usr/lib|g"		# Remap some paths for libtool cross linking
	$(MAKE) -C "$(PKG_VER)" DESTDIR="$(PATH_FS)" install-exec
	touch $^ "$@"


$(PKG_VER)/.nard-build: $(std-deps)
	$(call requireProg, nard-protoc)											# We need the host native protocol buffers compiler
	cd "$(PKG_VER)" && ./configure --prefix=/usr								\
		CFLAGS="$(CROSS_CFLAGS)"												\
		LDFLAGS="$(CROSS_LDFLAGS)"												\
		CPPFLAGS="$(CROSS_CFLAGS)"												\
		--sysconfdir=/etc														\
		--disable-dependency-tracking											\
		--disable-silent-rules													\
		--disable-static														\
		--with-protoc="$(PATH_UTIL)/bin/nard-protoc"							\
		--host=$(CROSS_TUPLE)													\
		--with-sysroot=$(PATH_SYSROOT)
	$(MAKE) -C "$(PKG_VER)" -j $(CPUS) V=1
	touch "$@"


$(PKG_VER)/.nard-extract: $(PKG_VER).tar.*
	$(std-extract)


#----------------------------													# Cleaning	
.PHONY: clean		
clean:
	$(std-clean)

.PHONY: distclean
distclean:
	$(std-distclean)

