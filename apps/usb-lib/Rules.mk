
# When building apps which use this library you need to include
# this Rules.mk file for getting the paths to headers and libs.
#
# 1) In your makefile, add a line like this:
#      include $(PATH_APPS)/xyz-lib/Rules.mk
# 2) Modify your compiler flags:
#      CFLAGS += $(CROSS_CFLAGS)



# Automatically define a path to the library this
# Rules.mk file belongs to, with the format
# $(PATH_APPS)/xyz-app/xyz-app
# Utilizes lib functions in the toplevel Rules.mk.
export PATH_USBLIB := $(curr_app)

CROSS_CFLAGS += -I$(PATH_USBLIB)/staging/usr/include/
CROSS_CFLAGS += -I$(PATH_USBLIB)/staging/usr/include/libusb-1.0
CROSS_CFLAGS += -L$(PATH_USBLIB)/staging/usr/lib
CROSS_CFLAGS += -Wl,-rpath-link,$(PATH_USBLIB)/staging/usr/lib

CROSS_LDFLAGS += -L$(PATH_USBLIB)/staging/usr/lib
CROSS_LDFLAGS += -Wl,-rpath-link,$(PATH_USBLIB)/staging/usr/lib

