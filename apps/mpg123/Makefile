
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2016 Ronny Nilsson


include ../../Rules.mk															# Common used stuff

# mpg123 needs ALSA library, so including
# Rules (paths) to salsa-lib staging area.
include $(PATH_APPS)/salsa-lib/Rules.mk


#-----------------------------													# Standard targets
.PHONY: all $(PKG_VER)
all: $(PATH_FS)/.nard-$(PKG_NAME)
$(PKG_VER): $(PATH_FS)/.nard-$(PKG_NAME)


.PHONY: install
install: $(PATH_FS)/.nard-$(PKG_NAME)
$(PATH_FS)/.nard-$(PKG_NAME): $(PKG_VER)/.nard-build
	$(MAKE) -C "$(PKG_VER)" DESTDIR="$(CURDIR)/$(PKG_VER)/staging" install
	$(MAKE) -C "$(PKG_VER)" DESTDIR="$(PATH_FS)" install-exec
	touch "$@"


$(PKG_VER)/.nard-build: $(std-deps)
	cd "$(PKG_VER)" && ./configure CFLAGS="$(CROSS_CFLAGS)"						\
		CPPFLAGS="$(CROSS_CFLAGS)" --prefix=/usr								\
		--sysconfdir=/etc --host=$(CROSS_TUPLE)									\
		--disable-dependency-tracking --disable-static							\
		--enable-shared=yes --disable-modules --enable-fifo=yes					\
		--enable-network=yes --disable-silent-rules								\
		--with-cpu=$(strip														\
				$(if $(CROSS_HAS_NEON),											\
					neon,generic_dither											\
				)																\
			)																	\
		--with-sysroot=$(PATH_SYSROOT) --with-audio=alsa						\
		--with-default-audio=alsa
	$(MAKE) -C "$(PKG_VER)" -j $(CPUS)
	touch "$@"


$(PKG_VER)/.nard-extract: $(PKG_VER).tar.*
	$(std-extract)



#----------------------------													# Cleaning	
.PHONY: clean		
clean:
	$(std-clean)

.PHONY: distclean
distclean:
	$(std-distclean)

