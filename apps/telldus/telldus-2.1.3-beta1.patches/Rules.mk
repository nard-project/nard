
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2018 Ronny Nilsson


# Check prerequisites
ifeq ($(PKG_VER),)
$(error Variable PKG_VER hasn't expanded correctly)
else ifeq ($(PKG_VER),$(PKG_NAME))
$(error Variable PKG_VER hasn't expanded correctly)
endif


ifndef $(PKG_NAME)-INST
	COMMON_FLAGS := -DVERSION=\"$(subst $(PKG_NAME)-,,$(PKG_VER))\"
	COMMON_FLAGS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE
	COMMON_FLAGS += -D_FILE_OFFSET_BITS=64 -D_LINUX
	COMMON_FLAGS += -I "$(PATH_APPS)/$(PKG_NAME)/$(PKG_VER)/telldus-core"
	COMMON_FLAGS += -L "$(PATH_APPS)/$(PKG_NAME)/$(PKG_VER)/telldus-core/client"
	COMMON_FLAGS += -L "$(PATH_APPS)/$(PKG_NAME)/$(PKG_VER)/telldus-core/common"

	CFLAGS := $(CROSS_CFLAGS)
	CFLAGS += -std=gnu89
	CFLAGS += $(COMMON_FLAGS)

	CXXFLAGS := $(CROSS_CXXFLAGS)
	CXXFLAGS += -std=gnu++98
	CXXFLAGS += $(COMMON_FLAGS)

	$(PKG_NAME)-INST := $(PATH_FS)/usr
endif

