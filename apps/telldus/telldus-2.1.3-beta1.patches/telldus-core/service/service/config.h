#ifndef CONFIG_H
#define CONFIG_H

#define CONFIG_PATH "/etc"
#define VAR_CONFIG_PATH "/var/lib/telldus"
#define SCRIPT_PATH "/usr/share/telldus/scripts"

#endif  // CONFIG_H
