
Provided is a script for generation a large number of configuration
files and key files for many clients at once.

First run the script:
	bash tinc-gen-conf-keys.sh
Then move each generated client directory to respective client /etc/tinc








---------------------------------------------------------
# Example start of tinc at boot:

# Check if keys and configuration are stored in
# the persistent SD-card storage /boot/settings
if [ ! -e "/etc/tinc" -a -e "/etc/settings/tinc" ]; then
	ln -s "settings/tinc" "/etc/tinc"
fi


# Ensure the secret key file has correct permissions.
# If it were stored in the persistend SD-card storage
# it was likely wrong and need a runtime fix.
chown -R root:root /etc/tinc/
find -L /etc/tinc/ -name ed25519_key.priv | xargs chmod 0600


# Load the kernel driver for the tunnel interface.
modprobe tun


# Sleep a small amount of random time to not overload
# the server if multiple clients start simultaneously.
random_number=$(strings /dev/urandom | grep -Eo -m6 "[1-9]" | \
	tr -d "\n" | head -c 6)
usleep $random_number


# Start all available configured VPNs
for vpnName in $(cd /etc/tinc && find * -maxdepth 0 -type d); do
	tinc -n ${vpnName} start
done

-------------------------------------------------------
