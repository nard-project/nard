#!/bin/bash

# Script for config+key generation for one server and many clients.
# Can be used for generating many client configurations from a
# single template. Those configurations can later be uploaded
# via scp to the target. tinc "invitations" are not used. Just
# generate the configs and keys, upload them to target and start
# the daemons. Can also be used in the factory to preparate lots of
# completely new SD cards, before the very first start of the
# Raspberry Pis. Just copy the generated files into respective
# SD card /settings/tinc



# IP subnet, for use with a /24 netmask
subnet=192.168.20.

# Start of subnet range. The server machine will get
# the first in the range.
suboctet=1

# MTU choosen to avoid fragments in this common scenario:
#  VPN interface > tinc daemon > Host VLAN > Host ethernet interface
# Relates to PMTU in tinc.conf!
mtu=1421


# List of clients. Only INCREASE this list! Never
# erase a name! Instead add old deleted names to
# <clients_revoked> below. Ensure to keep the
# initial whitespace of every name!
clients_list+=" ferrari"
clients_list+=" renault"
clients_list+=" mercedes"
clients_list+=" ford"
clients_list+=" volvo"

# List of denied clients. Leave these names intact
# in <clients_list> above or the IP address will
# become wrong.
clients_revoked+=" renault"
clients_revoked+=" ford"





#----------------------------------------------------------------------------------
PATH+=":/usr/local/sbin:/usr/sbin:/sbin"

if ! which tinc >/dev/null; then
	echo "Error, need tinc installed for generation of keys!"
	exit 1
fi


# Generate configuration for the server daemon.
echo "${subnet}${suboctet} is server"
mkdir -p "tinc-server/hosts"
cp -af tinc-template-server/* "tinc-server"
echo "Subnet = ${subnet}0/24" >>"tinc-server/hosts/server"
echo "ifconfig \$INTERFACE ${subnet}${suboctet} netmask 255.255.255.0 mtu $mtu up" >>"tinc-server/tinc-up"
suboctet=$((suboctet+1))

# Generate keys for server first time only.
# Must be done before the host configs are
# put into place or the file names will
# become wrong.
if [ ! -f "tinc-server/ed25519_key.priv" ]; then
	mkdir -p "tinc-server.tmp"
	tinc -bc "tinc-server.tmp" generate-ed25519-keys 2>&1 | \
		while read l; do echo "    ${l}"; done									# Indent tinc output with some spaces
	mv -f tinc-server.tmp/ed25519_key.* "tinc-server"
	rmdir "tinc-server.tmp"
fi
echo "-----BEGIN ED25519 PUBLIC KEY-----" >"tinc-server/hosts/server_ed25519_key.pub"
tail -c +20 "tinc-server/ed25519_key.pub" >>"tinc-server/hosts/server_ed25519_key.pub"
echo "-----END ED25519 PUBLIC KEY-----" >>"tinc-server/hosts/server_ed25519_key.pub"




# Generate configurations and keys for the clients.
for client in $clients_list; do
	echo "${subnet}${suboctet} is ${client}"
	mkdir -p "tinc-client-${client}/hosts"
	cp -af tinc-template-client/* "tinc-client-${client}"

	# Generate client keys if necessary and copy the public
	# into the server runtime config path.
	if [ ! -f "tinc-client-${client}/ed25519_key.priv" ]; then
		tinc -bc "tinc-client-${client}" generate-ed25519-keys 2>&1 | \
			while read l; do echo "    $l"; done								# Indent tinc output with some spaces
	fi
	echo "-----BEGIN ED25519 PUBLIC KEY-----" >"tinc-server/hosts/${client}_ed25519_key.pub"
	tail -c +20 "tinc-client-${client}/ed25519_key.pub" >>"tinc-server/hosts/${client}_ed25519_key.pub"
	echo "-----END ED25519 PUBLIC KEY-----" >>"tinc-server/hosts/${client}_ed25519_key.pub"

	# Copy the server key to the generated client.
	cp -af "tinc-server/hosts/server" "tinc-client-${client}/hosts"
	cp -af "tinc-server/hosts/server_ed25519_key.pub" "tinc-client-${client}/hosts"

	# Add unique name and IP address to each generated client.
	echo "Name = ${client}" >>"tinc-client-${client}/tinc.conf"
	mv -f "tinc-client-${client}/hosts/client" "tinc-client-${client}/hosts/${client}"
	echo "Subnet = ${subnet}${suboctet}" >>"tinc-client-${client}/hosts/${client}"
	echo "ifconfig \$INTERFACE ${subnet}${suboctet} netmask 255.255.255.0 mtu $mtu up" >>"tinc-client-${client}/tinc-up"

	suboctet=$((suboctet+1))
	if [ $suboctet -ge 256 ]; then
		echo "Error; to many clients for subnet!"
		exit 1
	fi

	# Copy generated client conf to server runtime and add
	# path to client public key.
	cp -af "tinc-client-${client}/hosts/${client}" "tinc-server/hosts/"
	echo "Ed25519PublicKeyFile = hosts/${client}_ed25519_key.pub" >>"tinc-server/hosts/${client}"
done


# Erase the keys for old revoked clients. This will
# make them unable to connect to the server any more.
for client in $clients_revoked; do
	echo "Revoking ${client}."
	rm -f "tinc-server/hosts/${client}_ed25519_key.pub" \
		"tinc-server/hosts/${client}"
done




echo "-----------------------------------------------------------------------------"
echo "Now MIRROR (not just copy)  tinc-server/  to your server machine:"
echo "    sudo rm -rf /etc/tinc/<net-name>"
echo "    sudo cp -af tinc-server /etc/tinc/<net-name>"
echo "    sudo chown -R root:root /etc/tinc"
echo
echo "Mirror each  tinc-client-xxx/  directory to their respective target SD card:"
echo "    - Via local SD card reader:"
echo "          rm -rf /mnt/sdcard/settings/tinc/<net-name>"
echo "          cp -af tinc-client-xxx /mnt/sdcard/settings/tinc/<net-name>"
echo "    - Via remote SSH:"
echo "          ssh target \"rm -rf /boot/settings/tinc/<net-name>\""
echo "          scp -pr tinc-client-xxx \"root@target:/boot/settings/tinc/<net-name>\""

