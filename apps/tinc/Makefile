
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2018 Ronny Nilsson


include ../../Rules.mk															# Common used stuff

# Include necessary librarys.
include $(PATH_APPS)/z-lib/Rules.mk
include $(PATH_APPS)/openssl-lib/Rules.mk


#-----------------------------													# Standard targets
.PHONY: all $(PKG_VER)
all: $(PATH_FS)/.nard-$(PKG_NAME)
$(PKG_VER): $(PATH_FS)/.nard-$(PKG_NAME)


.PHONY: install
install: $(PATH_FS)/.nard-$(PKG_NAME)
$(PATH_FS)/.nard-$(PKG_NAME): $(PKG_VER)/.nard-build $(std-deps)
	$(MAKE) -C "$(PKG_VER)" -j $(CPUS) DESTDIR="$(PATH_FS)" V=1 install-exec
	touch "$@"


$(PKG_VER)/.nard-build: $(PKG_VER)/.nard-extract Makefile
	test -x "$(PKG_VER)/configure" || (cd "$(PKG_VER)" && autoreconf -fiv)
	cd "$(PKG_VER)" && ./configure												\
		CFLAGS="$(CROSS_CFLAGS)"												\
		--host=$(CROSS_TUPLE) --target=$(CROSS_TUPLE)							\
		--prefix=/usr --sysconfdir=/etc --localstatedir=/var					\
		--disable-silent-rules --disable-curses --disable-readline				\
		--disable-lzo --with-zlib-include="$(PATH_ZLIB)"						\
		--with-zlib-lib="$(PATH_ZLIB)"											\
		--with-openssl-include="$(PATH_OPENSSL)/usr/include"					\
		--with-openssl-lib="$(PATH_OPENSSL)/usr/lib"							\
		--disable-legacy-protocol
	touch "$@"


$(PKG_VER)/.nard-extract: $(wildcard $(PKG_VER).tar.*)
	$(std-extract)



#----------------------------													# Cleaning	
.PHONY: clean		
clean:
	-[ -d "$(PKG_VER)" ] && $(MAKE) -C "$(PKG_VER)" DESTDIR="$(PATH_FS)" uninstall
	$(std-clean)

.PHONY: distclean
distclean:
	-[ -d "$(PKG_VER)" ] && $(MAKE) -C "$(PKG_VER)" DESTDIR="$(PATH_FS)" uninstall
	$(std-distclean)

