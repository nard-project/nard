protoc/ola_protoc_plugin$(EXEEXT): CXX := g++
protoc/ola_protoc_plugin$(EXEEXT): CFLAGS := -I"$(PATH_UTIL)/protobuf-compiler/protobuf-compiler/staging/include" -L"$(PATH_UTIL)/protobuf-compiler/protobuf-compiler/staging/lib"
protoc/ola_protoc_plugin$(EXEEXT): CPPFLAGS := -I"$(PATH_UTIL)/protobuf-compiler/protobuf-compiler/staging/include" -L"$(PATH_UTIL)/protobuf-compiler/protobuf-compiler/staging/lib"
protoc/ola_protoc_plugin$(EXEEXT): LDFLAGS := -L"$(PATH_UTIL)/protobuf-compiler/protobuf-compiler/staging/lib"
protoc/ola_protoc_plugin$(EXEEXT): LIBS := -lprotobuf
