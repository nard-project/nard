#include <stdio.h>
#include <time.h>
#include "includes.h"
#include "wpa_ctrl.h"

#define CTRL_PATH "/var/run/wpa_supplicant/wlan0"

int main(int argc, char** argv)
{
     struct wpa_ctrl* wpac;
    int ret;

    char* path = CTRL_PATH;

    setbuf(stdout, NULL);

    if (argc >= 2) {
        path = argv[1];
    } else {
        fprintf(stderr, "No wpa_supplicant CTRL_PATH given. Using default value of '%s'\n", CTRL_PATH);
    }

    wpac = wpa_ctrl_open(path);

    if (!wpac){
        fprintf(stderr, "Could not get ctrl interface: %s!\n", path);
        return -1;
    }

    if ((ret = wpa_ctrl_attach(wpac)) != 0) {
        fprintf(stderr, "Could not attach to ctrl interface. Error %d\n", ret);
        return -2;
    }

    while(1) {

        int count = wpa_ctrl_pending(wpac);
        int i;
        size_t sz;
        char reply[1000];
        char timestr[100];
        time_t rawtime;
        struct tm *tmp;

        for(i=0; i<count; i++) {
            sz = 1000;
            memset(reply, 0x00, 1000);
            memset(timestr, 0x00, 100);

            time( &rawtime );
            tmp = gmtime(&rawtime);

            strftime(timestr, 100, "%Y-%m-%d %H:%M:%S", tmp);

            if (wpa_ctrl_recv(wpac, reply, &sz) != 0) {
                fprintf(stderr, "Failed getting wpa message!\n");
            } else {
                printf("%s: %s\n", timestr, reply);
            }
        }

        fflush(stdout);

        os_sleep(0, 250000);
    }

    return 0;
}