SHAREDLIBSUFFIX = so
SHAREDLIBSUFFIXFULLVER=$(SHAREDLIBSUFFIX).$(FULL_VERSION)
SHAREDLIBSUFFIXMAJORVER=$(SHAREDLIBSUFFIX).$(SHAREDLIB_MAJORVERSION)
SHLDFLAGS = -Wl,-soname,$(LIBPREFIX)$(PROJECT_NAME).$(SHAREDLIBSUFFIXMAJORVER)
CFLAGS += -Wall -fno-strict-aliasing -fPIC -MMD -MP
ifeq ($(USE_STACK_PROTECTOR), Yes)
CFLAGS += -fstack-protector-all
endif
LDFLAGS += -lpthread
STATIC_LDFLAGS += -lpthread -lm
AR_OPTS = crD $@

ifeq ($(CXX), clang++)
CXXFLAGS += -Wc++11-compat-reserved-user-defined-literal
endif

ifeq ($(patsubst %g++,,$(CXX)),)
GCCVER_GTEQ8 = $(shell echo $$(($$($(CXX) -dumpversion | awk -F "." '{print $$1}') >= 8)))
ifeq ($(GCCVER_GTEQ8), 1)
CXXFLAGS += -Wno-class-memaccess
endif
endif


ifdef ARCH_ARM32
	ASM_ARCH = arm
	ASMFLAGS += -I$(SRC_PATH)codec/common/arm/

	ifneq ($(CROSS_HAS_NEON),)
		CFLAGS += -DHAVE_NEON
	endif
endif

ifdef ARCH_ARM64
	ASM_ARCH = arm64
	ASMFLAGS += -I$(SRC_PATH)codec/common/arm64/

	ifneq ($(CROSS_HAS_NEON),)
		CFLAGS += -DHAVE_NEON_AARCH64
	endif
endif

