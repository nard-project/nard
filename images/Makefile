
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2018 Ronny Nilsson


include ../Rules.mk																# Common used stuff



#-----------------------------													# Standard targets
.PHONY: all audit
all: sdcard.tar.gz sdcard.img.gz
audit: audit.tar.gz


# Create a tarball for upload to Nard TaaS regression testing. We
# perform absolute to relative path subsituion here for the object
# files to work on the server too, thus the complexity. Symlinks
# are forbidden (for security) and thus dereferenced.
audit.tar.gz: sdcard.img.gz
	@echo "Preparing Nard TaaS upload..." 
	for conf in $$(cd "$(PATH_TOP)/platform" &&									\
			find -L $(call reverse, $(PRODUCT_DEPS) $(BOARD_DEPS))				\
			-maxdepth 1 -name taas.config); do									\
		source "$(PATH_TOP)/platform/$${conf}";									\
	done;																		\
	echo $$taasSeqs >"$(PATH_SCRAP)/taasSeqs"									# Choosen test sequences from taas.config
	cd "$(PATH_FS)" &&															\
		for f in usr/bin/hello lib/libc.* lib/libc-* lib/ld-*; do				\
			if test -e "$$f"; then												\
				tar -rvhf  "$(PATH_IMAGES)/audit.tar" "$$f";					\
			fi;																	\
		done;																	# The Hello World example for GDB testing
	cd "$(PATH_UTIL)/.." && d="$(PATH_UTIL)" &&									\
		tar -rvhf "$(PATH_IMAGES)/audit.tar"									\
		$${d##"$(PATH_TOP)/"}/crosstool-ng/crosstool-ng-*/build*/src/glibc-*	# libc sources for GDB testing
	cd "$(PATH_APPS)/.." && d="$(PATH_APPS)" &&									\
		tar -rvhf "$(PATH_IMAGES)/audit.tar"									\
		$${d##"$(PATH_TOP)/"}/hello												# Hello World sources for GDB testing
	echo "$(PATH_TOP)" >"$(PATH_SCRAP)/pathtop"
	cd "$(PATH_SCRAP)" && tar -rvhf "$(PATH_IMAGES)/audit.tar" pathtop taasSeqs
	cd "$(PATH_INTER)" && tar -rvhf "$(PATH_IMAGES)/audit.tar"					\
		product_ssh_key product_ssh_key.pub
	tar -rvhf audit.tar sdcard.img.gz sdcard.tar.gz
	gzip --fast -f audit.tar


# Create a SD-card image which contains everything
# needed to boot a Raspberry Pi. The image is hard-
# coded with a 1 GB FAT32 partition (for GPU firmware
# and the Linux kernel) and an empty, very small second
# partition. The second partition will automatically
# be extended to full card size upon first boot.
sdcard.img.gz: sdcard.tar.gz
	cat mbr-1gb-fat32.dat >sdcard.img
	dd if=/dev/zero of=sdcard.img bs=512 count=31 seek=1 status=noxfer			# 15 sectors of padding betweend mbr and first partit
	test -n "$(PRODUCT)"
	$(PATH_UTIL)/mtools/mtools/mformat -i "sdcard.img@@8192"					\
		-h 255 -s 63 -T 2104452	-F -v $(PRODUCT) -M 512 -S 2					# Fat32 in first partit, 2104452 sectors 
	$(PATH_UTIL)/mtools/mtools/mcopy -sQmvi "sdcard.img@@8192"					\
		"$(PATH_BOOT)/"* ::
	gzip --fast -f sdcard.img


# Create a tarball with a new kernel + filesystem
# which we can transfer to a remote target by ftp/scp
# for easy updates.
FIRMW_BINS := $(shell															\
	for D in $(foreach DEP, $(call reverse, $(PRODUCT_DEPS) $(BOARD_DEPS)),		\
			$(PATH_TOP)/platform/$(DEP)/firmware-template); do					\
		test -e "$$D" && find -L "$$D" -mindepth 1;								\
	done;																		\
)
UPGRD_BINS := initramfs.cpio.gz.md5sum zImage.md5sum
UPGRD_BINS += initramfs.cpio.gz zImage
sdcard.tar.gz: $(addprefix $(PATH_BOOT)/, $(UPGRD_BINS))
sdcard.tar.gz: $(FIRMW_BINS) Makefile
	test -d "$(PATH_BOOT)" || mkdir -p "$(PATH_BOOT)"
	for D in $(foreach DEP, $(call reverse, $(PRODUCT_DEPS) $(BOARD_DEPS)),		\
			$(PATH_TOP)/platform/$(DEP)/firmware-template); do					\
		if test -e "$$D"; then													\
			$(CP) -fHLpRv "$$D/"* "$(PATH_BOOT)";								\
		fi;																		\
	done
	cat "$(PATH_BOOT)/config-bootcode.txt" "$(PATH_BOOT)/config-start.txt"		\
		>"$(PATH_BOOT)/config.txt"
	rm -f "$(PATH_BOOT)/config-bootcode.txt" "$(PATH_BOOT)/config-start.txt"	\
		"$(PATH_BOOT)/config-platforms.txt" "$(PATH_BOOT)/upgrade.md5sum"		\
		"dir.txt"
	for D in $(call reverse, $(PRODUCT_DEPS) $(BOARD_DEPS)); do					\
		echo "include config-$${D}.txt" >>"$(PATH_BOOT)/config-platforms.txt";	\
	done
	cd "$(PATH_BOOT)" && find -mindepth 1 -type f -printf "%P\n" >"dir.txt"		# Listing of all files, for PXE boot by TFTP.
	cd "$(PATH_BOOT)" &&														\
		find -mindepth 1 -type f ! -path "*/settings/*" -printf "%P\n" |		\
		xargs md5sum -b >"$(PATH_SCRAP)/upgrade.md5sum"
	mv -vf "$(PATH_SCRAP)/upgrade.md5sum" "$(PATH_BOOT)/upgrade.md5sum"
	cd "$(PATH_BOOT)" && tar --format=posix --owner 0 --group 0					\
		-cvzf "$(PATH_IMAGES)/$@" *



#----------------------------													# Cleaning	
.PHONY: clean		
clean:
	rm -f *.tar *.img *.gz

.PHONY: distclean
distclean:
	rm -f *.tar *.img *.gz

