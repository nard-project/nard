#!/bin/sh

# When this script is run the initramfs archive
# will soon be generated. There is a list of the
# files which will be included in the initramfs
# archive. All files in the list has default
# permission mode bits set. Some packages need
# custom mode bits though and this script will
# "override" the list defaults.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2019 Ronny Nilsson


set -e

# Check arguments
test -s "$1"
test -s "${PATH_SCRAP}/initramfslist"


# Create a big regular expression of the files
# that should override the defaults (those
# generated with gen_initramfs_list_fast.sh).
awk '$2 ~ /^\// {
	printf "^[[:alnum:]]+[[:blank:]]+%s[[:blank:]]|",$2;
}' "$1"	>"${PATH_SCRAP}/initramfslist-override"
echo "^endoflinedummynamenohit" >>"${PATH_SCRAP}/initramfslist-override"	# Closing alternate expression


# Filter away those entries in initramfslist
# that should be overridden.
echo -n >>"${PATH_SCRAP}/initramfslist"										# Ensure the file exists
grep -Evf "${PATH_SCRAP}/initramfslist-override" \
	"${PATH_SCRAP}/initramfslist" \
	>"${PATH_SCRAP}/initramfslist-temp1"


# Join filtered initramfslist with the overrider  
cat "${PATH_SCRAP}/initramfslist-temp1" "$1" \
	>"${PATH_SCRAP}/initramfslist-temp2"


# Filter away multiple consecutive whitespaces
# since they break later sorting.
sed -re  "s/[[:blank:]]{2,}/ /g" "${PATH_SCRAP}/initramfslist-temp2" \
	>"${PATH_SCRAP}/initramfslist-temp1"


# Ensure all directories (possibly with manualy
# set mode bits) come first. The cpio extractor
# might not be able to unpack a file into a
# non-existing directory.
grep -E "^dir" "${PATH_SCRAP}/initramfslist-temp1" | sort -bdis -k 99999 \
	>"${PATH_SCRAP}/initramfslist-temp2"
grep -Ev "^dir|^#|^$" "${PATH_SCRAP}/initramfslist-temp1" \
	>>"${PATH_SCRAP}/initramfslist-temp2"


# Finish and cleanup
mv -f "${PATH_SCRAP}/initramfslist-temp2" "${PATH_SCRAP}/initramfslist"
rm -f "${PATH_SCRAP}/initramfslist-temp1"


exit 0

