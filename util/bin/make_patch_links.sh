#!/bin/bash

####################################################
# MAKE PATCH LINKS
# Takes care of symlinking patched application files
# into source build tree (which is left outside
# revision control). It is assumed your patches exist
# in a directory parallell to the fresh-source-tree, 
# named equally but with ".pathes" suffis.
#
# USAGE:
# make_patch_links.sh <fresh-source-tree> <file1> <file2>..
#
# fresh-source-tree		Directory with unpacked tarball
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2018 Ronny Nilsson
####################################################


APPDIR="$1"
PATCHDIR="${1}.patches"
BASEDIR="$PWD"

# Check that prerequisites and argumetns looks OK
if [[ -d "$APPDIR" ]] && ! [[ -d "$PATCHDIR" ]] && [[ -n "$2" ]]; then
	echo "Can't find $PATCHDIR for making of symlinks!"
	exit 1
fi

[[ -d "$PATCHDIR" ]] || exit 0
[[ -z "$2" ]] && exit 0

if ! [[ -d "$APPDIR" ]]; then
	echo "Can't find $APPDIR for making of symlinks!"
	exit 1
fi


# Check that all patch files given on
# the command line exists and looks OK
shift
cd "$PATCHDIR" || exit 1
FILES_NEW=""
for FILE in $@; do
	if ! [[ -e "$FILE" ]]; then
		echo "Error, unknown patch file $PATCHDIR/$FILE"
		exit 1
	elif ! [[ -f "$FILE" ]] && ! [[ -L "$FILE" ]]; then
		echo "Error, only regular files allowed $PATCHDIR/$FILE"
		exit 1
	elif [[ "${FILE##*.}" = "patch" ]]; then
		echo "Error; use the patch tool for .patch files!  $FILE"
		exit 1
	fi

	FILES_NEW="${FILES_NEW} ${FILE##*\./}"												#  Remove leading "./" if any
done


# For each patch file given on the command line,
# create a symlink in the fresh source tree.
for KDIR in $FILES_NEW; do
	cd "$BASEDIR" || exit 1																# Makes both absolute and relative args work
	if [[ "${KDIR%/*}" != "${KDIR}" ]]; then											# Does path contain dir+file or only a file?
		[[ -d "${APPDIR}/${KDIR%/*}" ]] || \
			mkdir -vp "${APPDIR}/${KDIR%/*}" || exit 1									# Create dir where link should be created
		cd "${APPDIR}/${KDIR%/*}" || exit 1												# Go into dir were link should be created
	else
		cd "${APPDIR}" || exit 1
	fi

	FILE="${KDIR##*/}"																	# Get basename
	if ! [[ -L "$FILE" ]]; then															# Is there a link already?
		PART1="${FILE%\.?}"																# Name of file before "."
		PART2=".${FILE##*\.}"															# The name suffix

		[[ "$PART2" = ".$FILE" ]] && PART2="" 											# Not all files have suffixes	
		if ! [[ -e "${PART1}.orig${PART2}" ]] && [[ -e "$FILE" ]]; then					# Don't overwrite any existing file	
			mv -uv "$FILE" "${PART1}.orig${PART2}" || exit 1							# Save original backup
		fi

		if ! [[ -e "$FILE" ]]; then
			FILE_RELATIVE_PATH=""
			[[ "${KDIR%/*}" != "${KDIR}" ]] && \
				FILE_RELATIVE_PATH=$(echo "${KDIR%/*}" | sed -r -e "s/[^\/]+/../g")		# Get relative path to replacement file
			FILE_RELATIVE_PATH="../${FILE_RELATIVE_PATH}/${PATCHDIR}/${KDIR}"
			FILE_RELATIVE_PATH=$(echo "$FILE_RELATIVE_PATH" | sed -r -e "s/\/+/\//g")	# Remove multiple consecutive //

			if [[ -e "$FILE_RELATIVE_PATH" ]]; then										# Did we translate the relative path correctly?
				ln -vs "$FILE_RELATIVE_PATH" "$FILE" || exit 1							#  Don't create dangling symlinks.
			else
				echo "Bug, incorrect relative path $FILE_RELATIVE_PATH"
				exit 2
			fi
		else
			echo "Warning, conflicting patchfiles!"
			echo "$FILE"
			exit 2
		fi
	fi
done


