#!/bin/sh
#
# Example script for per device SD card provisioning. At the factory,
# when all SD cards are blank, run this script to:
#  - Write the Nard image to the card.
#  - Save a log of the card serial number.
#  - Write a device unique SSH key to the card.
#  - Write a device unique OpenVPN key to the card.
#  - Write a device unique hostname the card.
#  - Write a WiFi password to the card.
#
# Prerequisites:
#  - The computer must have "real" PCI based MMC card reader.
#  - You likely need to be root.


productID="100000"																# Assigned device unique serial number
pathMmcDev="/dev/mmcblk0"														# SD card block device in PC
pathMmcInfo=$(realpath /sys/bus/mmc/drivers/mmcblk/mmc0:*)						# Kernel MMC device info in PC
pathMnt="/mnt/rpi"																# Mount point of SD card in PC
pathArchive="secrets-${productID}"												# Archived copy of secret keys. Ensure to save this to your database!


# Exit on any error
set -e

# Sanity checks
if [ $(id -u) != "0" ]; then
	echo "Error, need to be root!"
	exit 1
fi

if [ ! -b "$pathMmcDev" ]; then
	echo "Error, no MMC block device. Insert a SD card!"
	exit 1
fi


# Find out the product name
a=$(grep PRODUCT intermediate/product.mk | head -n 1)
product=${a##* }


echo "Configuring product ID $productID"
mkdir "${product}-${pathArchive}"


# Read serial number of SD card
read -t 5 -s mmcSerial <"${pathMmcInfo}/serial"
read -t 5 -s mmcName <"${pathMmcInfo}/name"
read -t 5 -s mmcDate <"${pathMmcInfo}/date"
read -t 5 -s mmcOem <"${pathMmcInfo}/oemid"
read -t 5 -s mmcCid <"${pathMmcInfo}/cid"
(echo "SD card info: "				&&
echo "  serial ${mmcSerial}" 		&&
echo "  name   ${mmcName}"			&&
echo "  date   ${mmcDate}"			&&
echo "  OEM ID ${mmcOem}"			&&
echo "  CID    ${mmcCid}"			) | \
	tee "${product}-${pathArchive}/sdcard-info.txt"


echo "Uploading image to SD card..."
zcat "images/sdcard.img.gz" >$pathMmcDev
sync $pathMmcDev
partprobe $pathMmcDev


echo "Mounting SD card..."
[ -d "${pathMnt}" ] || mkdir -p "${pathMnt}"
mount -t vfat "${pathMmcDev}p1" "${pathMnt}" \
	-o codepage=850,conv=binary,iocharset=iso8859-1,discard


echo "Provisioning hostname..."
echo 'hostname="'${product}${productID}'"' >>"${pathMnt}/settings/hostname"
echo 'nodeid="'${productID}'"' >>"${pathMnt}/settings/nodeid"


echo "Provisioning WiFi password..."
echo 'wlan0_ssid="mywifinet"' >>"${pathMnt}/settings/network"
echo 'wlan0_psk="mypassword"' >>"${pathMnt}/settings/network"


echo "Provisioning VPN key..."
/usr/sbin/openvpn --verb 2 --genkey --secret "openvpn-${productID}.key"			# Discardable key, only for example
cp "openvpn-${productID}.key" "${pathMnt}/settings/openvpn.key"
mv "openvpn-${productID}.key" "${product}-${pathArchive}/"


echo "Provisioning SSH key..."
ssh-keygen -q -t rsa -v -N "" -f "ssh-${productID}.key"
cat "ssh-${productID}.key.pub" >>"${pathMnt}/settings/root_ssh_authorized_keys"
mv ssh-${productID}.key* "${product}-${pathArchive}/"


umount "${pathMnt}"
sync $pathMmcDev
echo "Finished OK"

